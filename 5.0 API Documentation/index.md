5.0 API Documentation
=====================

The current standard for documenting new REST APIs within T-Mobile is to use [Open API Specification 3.0](https://github.com/OAI/OpenAPI-Specification/blob/master/versions/3.0.3.md). We have large number of [Swagger2.0](https://github.com/OAI/OpenAPI-Specification/blob/master/versions/2.0.md#user-content-swaggerHost) APIs that can be migrated to OAS3 as soon as domain development tools and processes can support them. The Swagger specification has been renamed as the OpenAPI Specification, but an individual document for defining an API is still commonly referred to as a 'Swagger'. Also, the related open-source tools are still named for Swagger, such as [Swagger Editor](http://editor.swagger.io), [Swagger UI](https://swagger.io/swagger-ui/) and [Swagger Codegen](https://github.com/swagger-api/swagger-codegen).

> **Warning**
> 
> Do not use online editors when developing sensitive APIs or those associated with sensitive projects such as UnCarrier moves. Instead edit files locally or use T-Mobile hosted editors such as the [on-prem T-Mobile hosted Swagger Editor](https://swaggereditor.geo.pks.t-mobile.com/).

Swagger files can be written in YAML or JSON. The support for the newer version of the specification [OpenAPI 3.0](https://github.com/OAI/OpenAPI-Specification/blob/master/versions/3.0.0.md) has been added in all shared API ecosystem tools and platforms.

#### x-api-pattern

To identify which pattern a given API is implementing, each API in a Swagger file must specify the custom \`x-api-pattern' field with one of the values found in the [API Patterns](../../sites/linked/apidesignguidelines/http-method-usage/api-patterns) table. This custom swagger field identifies which API pattern to enforce during validation of API elements such as headers and status codes.

It is specified as shown in the following Swagger excerpt:

           '/customer/v1/accounts/{account-id}':
                get:
                 x-api-pattern: QueryResource
                 tags:
                  - Accounts
                 summary: Returns the customer account corresponding to the specified account-id

Note that the 'x-api-pattern' field is used only by T-Mobile and ignored by all other Swagger tools.

API Categorization Axis
-----------------------

 [API Categorization](https://gitlab.com/tmobile/securitysolutions/assessmentscertifications/requirementsdocumentation/-/blob/master/CategorizationFramework.md "API Categorization") helps drive API Design, Security, DevOps  and Lifecycle Governance decisions. [API Categorization PDF](https://tmobileusa.sharepoint.com/:b:/r/sites/SecuritySolutions/Shared%20Documents/Foundation/Categorization%20Framework%20Final.pdf?csf=1&web=1&e=h1JaMv "API Categorization PDF")​

Use below three fields to capture categorization in the Open API Specification document.

**x-data-sensitivity**, **​x-type-of-consumer** and **x-breadth-of-consumption**.

All API Categorization fields supports Open API Spec's document level configuration (part of "**info**" object), but document level values can be overriden at “**paths**” level (i.e. for each API operation), where paths level values takes precedence.

info:  
  description: An awesome API supporting critical business flows...  
  ...  
  x-type-of-consumer: EU  
  x-breadth-of-consumption: ANY  
  x-data-sensitivity: RESTRICTED  
  ...  
paths:  
  "/device/all-devices":  
    get:  
      tags:  
      - device  
      x-api-pattern: QueryCollection  
      x-data-sensitivity: Public  
…  
    post:  
      tags:  
      - device  
      x-api-pattern: CreateInCollection  
      x-type-of-consumer: ISD  
      x-breadth-of-consumption: FC  
      x-data-sensitivity: CONFIDENTIAL  
…  
    patch:  
      tags:  
      - device  
      x-api-pattern: ModifyResource  
…  
…​

#### x-data-sensitivity

Data flowing through the enterprise should be tagged with information about its security sensitivity. This can be used by the API Gateway and Data Loss Protection (DLP) systems to help control access. To identify the data sensitivity, each API in a Swagger file must specify the custom \`x-data-sensitivity' field For instance, if a an API is tagged with "x-data-sensitivity: Confidential" in it's specification, the API Gateway may inject policy to block the response from any external caller using Anonymous tokens.

[TISS Information classification](https://tmobileusa.sharepoint.com/sites/policies/TMUS%20Policies/TISS-310%20Information%20Classification%20Standard.pdf) is used as source to derive the possible values, use one of: Restricted, Confidential, Internal, Public. Select the highest classification value in the tag, in cases when API handles data of mixed classification.

It is specified as shown in the following Swagger excerpt. 

           '/customer/v1/accounts/{account-id}':
                get:
                 x-api-pattern: QueryResource
                 x-data-sensitivity: Restricted             tags:
                  - Accounts
                 summary: Returns the customer account corresponding to the specified account-id

This can additionally also be tagged in the "defintiions" section at an element and/or object level, if there is a need to designate more granular level data sensitivity. Same concept of priority hierarchy, more specific tag overrides definitions that are at higher level in object tree, applies:

...  
...

definitions:
  id:
    type: string
    format: uuid
    description: The unique identifier for an object
    example: 30f726f5-e3c8-4fe8-9f79-b1bab5c111af
    x-data-sensitivity: INTERNAL
  digits:
    type: object
    required:
    - npa
    - nxx  
    - ngp       
  x-data-sensitivity: PUBLIC
    properties:
      npa:
        type: string
        example: '720'
        description: The Numbering Plan. In North America this is the area code
        pattern: "\\\\d\\\\d\\\\d"
      nxx:
        type: string
        example: '391'
        description: The exchange code of an American Phone Number
        pattern: "\\\\d\\\\d\\\\d"
        x-data-sensitivity: RESTRICTED   
      ngp:   
        type: string   
        example: 'ATL'   
        description: The Phone Number group   
        pattern: "`^([a-zA-Z].*?){3}`"   
...
  
...  

Note that the 'x-data-sensitivity' field is used only by T-Mobile and ignored by all other Swagger tools.

#### x-type-of-consumer

The type of consumer (ToC) axis classifies an API based on who consumes it. While this axis is conceptually nominal, it can also be viewed as an interval scale that measures the distance between a consumer and T-Mobile, including policies and processes. Security controls and Gateway patterns that should be used, depends on the ToC for which an APIs target consumers.

To identify the a Type of Consumer, each API in a Swagger file must specify the custom \`x-type-of-consumer\` field.

For instance, if an API is tagged with \`x-type-of-consumer: IDD\` in it's specification, the API Gateway must be used.

The possible values, use one of (low to high): ISD, IDD, P, IV, EU. Select the highest internal scale value, in cases when API has mixed ToC.

ISD: Internal, Same Domain  
IDD: Internal, Different Domain (These two values separate internal consumers of your API based on whether they exist within the same team (or, put another way, the consuming application exists in the same capability domain bounded context as the API it uses). This line allows you to separate APIs from what would more properly be termed services.  
P: Partners  
IV: Independent Vendors(Such as Software Vendors etcetera. This category includes companies such as Apple, Google, and Microsoft, as well as industry disruptors and start-ups (e.g., Uber, Lyft, Fiverr). IV are more often than not treated equivalent to End Users from a governance perspective.  
EU: End Users

This category applies to any technically oriented end user as well as any API used by a single-page application (SPA) or mobile application, as the source code of these applications is readily available for inspection and reverse engineering by end users. 

Note that in this framework we govern API’s with ToC of Partners, Independent Vendors and End Users as equivalent, yet we’ve left them in this ToC list so that we can think about them individually.

It is specified as shown in the following Swagger excerpt. 

       '/customer/v1/accounts/{account-id}':  
            get:  
             x-api-pattern: QueryResource  
             x-data-sensitivity: Restricted  
             x-type-of-consumer: **IDD**  
             tags:  
              - Accounts  
             summary: Returns the customer account corresponding to the specified account-id

#### Note that the \`x-type-of-consumer\` field is used only by T-Mobile and ignored by all other Swagger tools.

#### x-breadth-of-consumption

The breadth of consumption (BoC) axis classifies an API based on how many consumers will adopt it. This axis is strictly an interval scale; the higher an API is on this axis, the more consumers it has, and thus the stricter the governance prescription must be to ensure that security issues don't become a significant risk to T-Mobile.   
Note that breadth of consumption refers to the number of unique consumers – it doesn't refer to call volume, because the majority of the cost in an API is a fixed per-consumer cost reflecting interactions with the development and support teams. 

To identify the a Breadth of Consumption, each API in a Swagger file must specify the custom \`x-breadth-of-consumption\` field. 

For instance, if an API is tagged with \`x-breadth-of-consumption: ANY\` in it's specification, other than impacting list of applicable security controls; the API productization, listing in external Marketplace and consumer outreach strategy has to be put in place.

The possible values, use one of (low to high): SC, FC, MC, ANY. Select the highest internal scale value, in cases when API has mixed BoC.

SC: Single consumer   
FC: Few consumers (2-5)  
MC: Many consumers (6-20)  
ANY: Anyone (20+)​

​

This value should be used for any API where there are effectively no restrictions on the breadth of consumption; you want this API to be adopted by as many consumers as possible. API additionally available on public open marketplace/portal.

It is specified as shown in the following Swagger excerpt. 

       '/customer/v1/accounts/{account-id}':  
            get:  
             x-api-pattern: QueryResource  
             x-data-sensitivity: Restricted  
             x-breadth-of-consumption: MC  
               
             tags:  
              - Accounts  
             summary: Returns the customer account corresponding to the specified account-id

#### Note that the \`x-breadth-of-consumption\` field is used only by T-Mobile and ignored by all other Swagger tools.

#### Field names

Fields should be named as instructed on the [Representation Design](../../sites/linked/apidesignguidelines/representation-design) page. These guidelines are applicable whether we were to use Swagger or some other API definition standard.

#### Swagger-specific guidelines

The following guidelines are specific to the use of Swagger 2.0. This is in addition to conforming to the 2.0 version of the specification. A valid Swagger 2.0 file will not produce any schema errors when loaded in the  [on-prem T-Mobile hosted Swagger Editor](https://swaggereditor.geo.pks.t-mobile.com/).

**Warning**

OAS3 should also support these but this section will be updated for additional OAS3 specific behavior.

#### Description fields

Be as verbose as possible in description fields. Verbose description fields are preferable to comments in YAML.

#### Field type and format

Swagger 2.0 accepts the [JSON Schema 04 Primitive Types](https://tools.ietf.org/html/draft-zyp-json-schema-04#section-3.5) to specify the `type` of a field:

| Field Type | Description |
| --- | --- |
| array | a JSON array |
| boolean | a JSON boolean |
| integer | a JSON number without a fraction or exponent part |
| number | any JSON number; number includes integer |
| object | a JSON object |
| string | a JSON string |

An optional `format` can be specified with the `type` to further define the contents of the field. Supported formats for each type are described in [Swagger 2.0 Data Types](https://github.com/OAI/OpenAPI-Specification/blob/master/versions/2.0.md#data-types).

| Common Name | Type | Format | Comments |
| --- | --- | --- | --- |
| integer | `integer` | `int32` | signed 32 bits  
use minimum, maximum |
| long | `integer` | `int64` | signed 64 bits  
use minimum, maximum |
| float | `number` | `float` | use minimum, maximum |
| double | `number` | `double` | use minimum, maximum |
| string | `string` |   | use minLength, maxLength and pattern (regular expression) |
| byte | `string` | `byte` | base64 encoded characters  
use minLength, maxLength |
| binary | `string` | `binary` | any sequence of octets, typically used to specify a binary file  
use minLength, maxLength, pattern (regular expression) |
| boolean | `boolean` |   | `true` or `false` |
| date | `string` | `date` | as defined by ‘full-date’ in [RFC3339](https://tools.ietf.org/html/rfc3339#section-5.6)  
for example, ‘2018-06-23’ |
| date-time | `string` | `date-time` | as defined by ‘date-time’ in [RFC3339](https://tools.ietf.org/html/rfc3339#section-5.6)  
for example, ‘2018-04-19T23:20:50.52Z’  
always return **UTC** time – not the time zone where the server is located |
| password | `string` | `password` | hint to UIs to obscure input  
use minLength, maxLength and pattern (regular expression) |

For each field, specify the type and format that best represents the field. A decimal value could be defined as follows:

          roomTemperatureFahrenheit:
            description: "The room temperature in degrees Fahrenheit"
            type: "string"

but it should be more specific:

          roomTemperatureFahrenheit:
            description: "The room temperature in degrees Fahrenheit"
            type: "number"
            format: "float"
            minimum: "-40.0"
            maximum: "130.0"

(Note that in addition to the primitive types listed above, Swagger 2.0 supports the primitive type `file` for some objects.)

Use integer/number/date/dateTime/boolean where appropriate along with enum / minimum / maximum / maxLength / minLength etc as appropriate, instead of string.

For type="string" always have a pattern, ideally fully describing the expected pattern but at least just the expected characters.

### Documenting Sensitive data in API Contract:

Agreement between a client and server as to what data is encrypted is crucial.  The best place to document this information is within the Swagger specification of the API.  Use format=encrypted to identify sensitive information which is encrypted and set x-tmo-cipher-format to the value of the original format property (before encryption).  Code generation tools such as SwaggerCodeGen (when using the T-Mobile Plugin for Swagger Code Generator) can be used to help encrypt and decrypt data.

Swagger encryption annotation:  
"Customer": {  
    "type": "object",  
    "properties": {  
        "birthdate": {  
            "description": "Birthdate of the customer",  
            "type": "string",  
            "format": "encrypted",  
            "x-tmo-cipher-format": "date"  
        }  
    }  
}

### Required Fields

When possible, build APIs with required fields rather than optional ones. An abundance of optional fields may indicate that the API is attempting to serve multiple purposes. When an API is ambiguous and composed of many optional fields, consider breaking it up by introducing additional APIs.

The following Contact type defines required `name` and `mobileNumber` fields and an optional `address` field:

      Contact:
        type: object
        required:
        - name
        - mobileNumber
        properties:
          name:
            $ref: '#/definitions/Name'
          mobileNumber:
            $ref: '#/definitions/MobileNumber'
          address:
            $ref: '#/definitions/MailingAddress'

### String and number minimums and maximums

To make an interface more specific, use `minimum` and `maximum` for numeric values and `minLength` and `maxLength` for strings. Regardless of whether there is an underlying limitation (such as database field size), restrict the API, according to its design, to better guard against injection and fuzzing attacks.

Attackers may attempt to send numbers larger than an API can handle or there may be confusion with signed or unsigned values.

A signed 32-bit integer allows numbers in the following range: minimum: -2147483648 maximum: 2147483647

Instead of allowing all these values, limit the range to values that make sense, such as in the following example:

      CreditTier:
        type: object
        properties:
          name:
            type: string
            maxLength: 12
            description: "The name of the credit tier."
          requiredCreditScore:
            type: integer
            format: int32
            minimum: 0
            maximum: 850
            description: "The required FICO score for this credit tier."

### String pattern with regex

If it is known that a string field must be strictly limited to a specific pattern, include a regular expression so that this limitation of your API is declared in the specification. For example, suppose an API only understands mobile numbers that are

*   10-digits in length
*   with the first digit of the area code not equal to 0 or 1
*   with the first digit of the NXX not equal to 0 or 1
*   with all other digits allowed to be 0 through 9

Define the field with a `pattern` specified as follows:

          mobileNumber:
            type: string
            description: The customer's phone number.
            pattern: '^[2-9]{1}\d{2}[2-9]{1}\d{6}$'
            example: 2068398685

If the field cannot be explicitly defined, then, to prevent attacks, the pattern must still indicate the allowed characters, minLength and maxLength.

          identifier:
            type: string
            minLength: 3
            maxLength: 15
            pattern: ' ^([A-Z][a-z][0-9]\/){3,15}$'

The above pattern uses the ^ (beginning of string) and $ (end of string) to ensure only the values specified in the regular expression are matched. In this case only upper case letters, lower case letters, digits and '/' characters are allowed; the minimum length is 3, and the maximum length is 15.

For more assistance with building regular expressions see [RegExr](https://regexr.com/) and [Regex101](https://regex101.com/).

### Enums

Use enum for a field that has a static set of valid values. For example:

               directionalPrefix:
               type: string
                description: The optional direction that may precede a street name in an address.
                enum: [N, S, E, W, NE, NW, SE, SW]

Other static values might include: state codes in the USA, month names, weekday names, etc.

Business values may also be represented with an enum. This would allow the addition of new values without breaking client code but is not recommended if existing enum values might be removed. For example, the following definition would break current clients if we wanted to remove 'sprint' as an eligible source when offering a promotion to customers to switch to T-Mobile:

            previousCarrier:
            type: string
            description: The carrier the customer is leaving to join T-Mobile.
            enum:
              - att
              - verizon
              - sprint

If possible values for a given field are limited but known to be dynamic in nature -- for example, there is an admin UI or some other mechanism for defining acceptable values, then avoid using an enum. Simply use a string and validate all values at the server.

> **Tip**
> 
> If the set of valid values for a given field are likely to change frequently, then avoid restricting the field with an enum as this will prevent loose coupling between the client and server.

#### Example values

Take advantage of the ability in Swagger to provide [example values](https://swagger.io/docs/specification/2-0/adding-examples/) in your specification. Examples values, such as for the object defined below, provide advantages such as:

*   allow Swagger UI to display sample data
*   facilitate automated generation of stubs for service virtualization and testing
    
        OrderLine:
        type: object
        properties:
          id:
            type: integer
            format: int32
            description: "The unique id of the line within the order."
            minimum: 1
            maximum: 99999
          sku:
            type: string
            maxLength: 10
            description: "The Stock Keeping Unit, the distinct type of an item."
            pattern: ' ^([A-Z][a-z][0-9]\/){3,10}$'
          quantity:
            type: integer
            format: int32
            minimum: 1
            maximum: 2147483647
            description: "How many of this item are included in the order."
        example: # example OrderLine
          id: 3
          sku: MNAA2LL/A
          quantity: 2
    
    OAS3 Overview
    -------------
    
    ### ![](./1595464998569-Slide19.PNG)
    
    *   ### Migration Options
        
    *   Convert :
    *   [https://mermade.org.uk/openapi-converter](https://mermade.org.uk/openapi-converter)
    *   POST: [https://oas-lint.apps.px-prd02.cf.t-mobile.com/api-center-for-enablement/v1/oas-lint/](https://oas-lint.apps.px-prd02.cf.t-mobile.com/api-center-for-enablement/v1/oas-lint/convert)[convert](https://oas-lint.apps.px-prd02.cf.t-mobile.com/api-center-for-enablement/v1/oas-lint/convert)
    
    *   Revised score impact api:
    *   GET: [https](https://oas-lint.apps.px-prd02.cf.t-mobile.com/api-center-for-enablement/v1/oas-lint/%7bOAS-id%7d)[://oas-lint.apps.px-prd02.cf.t-mobile.com/api-center-for-enablement/v1/oas-lint/{OAS-id}](https://oas-lint.apps.px-prd02.cf.t-mobile.com/api-center-for-enablement/v1/oas-lint/%7bOAS-id%7d)
    
    *   ### Below Ecosystem tools now support OAS 3
        
    *   Linter Engine & APIs
    *   Linter UI : [https](https://api-linter.devcenter.t-mobile.com/)[://api-linter.devcenter.t-mobile.com](https://api-linter.devcenter.t-mobile.com/)[/](https://api-linter.devcenter.t-mobile.com/)
    *   API ProxyGen & pipeline: [https://devcenter.t-mobile.com/devJourney?artifactName=apigeeAPI](../../devJourney?artifactName=apigeeAPI)
    *   Linter dashboard : [https](https://swagger_score_dashboard.apps.px-prd04.cf.t-mobile.com/)[://swagger\_score\_dashboard.apps.px-prd04.cf.t-mobile.com](https://swagger_score_dashboard.apps.px-prd04.cf.t-mobile.com/)[/](https://swagger_score_dashboard.apps.px-prd04.cf.t-mobile.com/)
    *   Dev Portal : [https://](https://developer.t-mobile.com/)[developer.t-mobile.com/](https://developer.t-mobile.com/)
    *   API Explorer : [https](https://api-explorer.devcenter.t-mobile.com/)[://api-explorer.devcenter.t-mobile.com](https://api-explorer.devcenter.t-mobile.com/)[/](https://api-explorer.devcenter.t-mobile.com/)
    *   Recite : [https](https://tmobileusa.sharepoint.com/teams/ServiceDesignOffice/RECITE%20Wiki/Home.aspx)[://tmobileusa.sharepoint.com/teams/ServiceDesignOffice/RECITE%20Wiki/Home.](https://tmobileusa.sharepoint.com/teams/ServiceDesignOffice/RECITE%20Wiki/Home.aspx)[aspx](https://tmobileusa.sharepoint.com/teams/ServiceDesignOffice/RECITE%20Wiki/Home.aspx)
    
    ### References and links
    
    *   OAS 3 spec: [https://github.com/OAI/OpenAPI-Specification/blob/master/versions/3.0.2.md](https://github.com/OAI/OpenAPI-Specification/blob/master/versions/3.0.2.md)
    *   OAS 2 spec: [https://github.com/OAI/OpenAPI-Specification/blob/master/versions/2.0.md](https://github.com/OAI/OpenAPI-Specification/blob/master/versions/2.0.md)
    *   SmartBear References: [https://swagger.io/docs/specification/about/](https://swagger.io/docs/specification/about/)
    *   Good comparison Blog: [https://blog.readme.io/an-example-filled-guide-to-swagger-3-2/](https://blog.readme.io/an-example-filled-guide-to-swagger-3-2/)
    *   Validation rule API: POST [https://oas-lint.apps.px-prd02.cf.t-mobile.com/api-center-for-enablement/v1/oas-lint/](https://oas-lint.apps.px-prd02.cf.t-mobile.com/api-center-for-enablement/v1/oas-lint/rule)[rule](https://oas-lint.apps.px-prd02.cf.t-mobile.com/api-center-for-enablement/v1/oas-lint/rule)
    *   Score impact to API: GET [https://oas-lint.apps.px-prd02.cf.t-mobile.com/api-center-for-enablement/v1/oas-lint](https://oas-lint.apps.px-prd02.cf.t-mobile.com/api-center-for-enablement/v1/oas-lint/%7bOAS-id%7d)[/{OAS-id}](https://oas-lint.apps.px-prd02.cf.t-mobile.com/api-center-for-enablement/v1/oas-lint/%7bOAS-id%7d)
    *   Repository Historical-Trend: GET [https://oas-lint.apps.px-prd02.cf.t-mobile.com/api-center-for-enablement/v1/oas-lint/repo/EITCODEDOC/](https://oas-lint.apps.px-prd02.cf.t-mobile.com/api-center-for-enablement/v1/oas-lint/repo/EITCODEDOC/historical-change-trend)[historical-change-trend](https://oas-lint.apps.px-prd02.cf.t-mobile.com/api-center-for-enablement/v1/oas-lint/repo/EITCODEDOC/historical-change-trend)
    *   Covert OAS2 to OAS3: [https://oas-lint.apps.px-prd02.cf.t-mobile.com/api-center-for-enablement/v1/oas-lint/](https://oas-lint.apps.px-prd02.cf.t-mobile.com/api-center-for-enablement/v1/oas-lint/convert)[convert](https://oas-lint.apps.px-prd02.cf.t-mobile.com/api-center-for-enablement/v1/oas-lint/convert)