[](#13-sensitive-identifiers)1.3 Sensitive Identifiers
======================================================

[](#131-the-security-issue)1.3.1 The Security Issue
---------------------------------------------------

It is common to identify resources within a domain with a piece of confidential information such as an account ID or a mobile number. Doing so would result in a URI as follows:

`https://api.t-mobile.com/billing/v2/subscribers/2068398685`

with the subscriber's mobile number as the final element.

This is unacceptable according to T-Mobile security guidelines. The table on page 5 of [TISS-310 Information Classification Standard](https://tmobileusa.sharepoint.com/sites/policies/TMUS%20Policies/TISS-310%20Information%20Classification%20Standard.pdf), specifies that Personally Identifying Information such as these are classified as T-Mobile Confidential information:

*   Customer name
*   TMOID
*   MSISDN
*   Billing Account Number (BAN)
*   IMEI
*   SIM
*   Email address

Confidential information must be handled as specified in table 3.2 of [TISD-1000 Information Handling Procedure](https://tmobileusa.sharepoint.com/sites/policies/TMUS%20Policies/TISD-1000%20Information%20Handling%20Procedure.pdf)

[](#132-a-secure-solution)1.3.2 A Secure Solution
-------------------------------------------------

To address this, confidential identifiers can be passed in the body of the HTTPS request rather than in the URI. This means that instead of finding the resource as follows:

'GET /billing/v2/subscribers/{mobile-number}\`

we would first call a search API and pass the subscriber safely within the message body:

    POST /billing/v2/subscribers/search
    {
       "mobileNumber": "2068398685"
    }

The server could return a response as follows using an identifier that is not sensitive like subscriberId:

    [
      {
      "subscriberId": "/billing/v2/subscribers/c13dfcdd-179c-4f32-8985-9d40eefc4ea0"
      }
    ]

If no resource matching the search criteria is found, then the response returns an empty array.

The non-sensitive identifier returned in the response can subsequently be used to access related resources and sub-resources without revealing sensitive information in the URI.

[](#alternate-solutions)Alternate Solutions
-------------------------------------------

If it is not possible for the server to return a URI without sensitive information as shown above, then there are other ways to address this issue:

#### [](#explicit-encryption-of-the-identifier)Explicit Encryption of the Identifier

If the identifier is encrypted in place within the URI, then the sensitive information will be safely transmitted. The downside to this is the effort involved in coordinating the additional level of encryption between the client and server.

#### [](#use-post-whenever-accessing-the-resource)Use POST whenever Accessing the Resource

Rather than just using the POST for the initial search, POST could be used for all access to the resource, always passing the sensitive identifier in the request body. It would be best to avoid this solution as it is a departure from REST and negates its benefits.