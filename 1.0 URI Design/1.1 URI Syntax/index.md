1.1 URI Syntax
==============

According to RFC 3986, the URI is made up of several parts:

> "The generic URI syntax consists of a hierarchical sequence of components referred to as the scheme, authority, path, query, and fragment."

For T-Mobile APIs, the components contain the following values:

| Component | Standard URI Format | T-Mobile Format | Comment | Examples |
| --- | --- | --- | --- | --- |
| scheme | \[scheme\]:// | \[scheme\]:// |   | https:// |
| authority | \[host\]\[:port\] | \[env\]\[nn\].\[api.t-mobile.com\] | env = dev/test
nn = environment number

port is optional

 | dev05.api.t-mobile.com

test01.api.t-mobile.com

api.t-mobile.com

 |
| path | /\[path\] | /\[package\]/v\[major-version\]/\[resource\]/\[sub-resource\] | zero or more sub-resources | `/billing/v2/accounts/{account-id}/billing-address` |
| query | ?\[key1=value1\]\[&key2=value2\] | ?\[key1=value1\]\[&key2=value2\] | used for filtering, sorting and paging | (see below) |
| fragment | #\[fragment-name\] | N/A | for use by user-agent only |   |

Combining all components, results in this:

    https://[env][nn].api.t-mobile.com/[package]/v[major-version]/[resource]/[sub-resource]?key1=value1&key2=value2

2.1.1 Package
-------------

The package represents a capability sub-domain. Capability sub-domains are defined as the L2 capabilities found the Business Capability Model (BCM). As of 9/5/17, BCM 4.0 is the current model version. BCM will continue to evolve in support of ongoing domain strategies and designs. Package names will follow spinal case to be consistent with the other URI components (i.e. resource).

| Organizational Domain | BCM - L2 Capability | Corresponding Package Name |
| --- | --- | --- |
| Product & Services | 
*   Product/Service Strategy & Planning
*   Product & Service Delivery
*   Product & Service Lifecycle Mgmt
*   Product & Service Performance Mgmt

 | 

*   product-strategy
*   product-delivery
*   product-lifecycle
*   product-performance

 |
| Billing & Charging Mgmt | 

*   Manage Billable Events
*   Charging Management
*   Billing Management
*   Billing Quality Assurance
*   Network Number Inventory Mgmt

 | 

*   mediations
*   rating-charging
*   billing
*   billing-qa
*   number-mgmt

 |
| Payments Mgmt | 

*   Card Payment Services Mgmt
*   Settlement & Exception
*   Services Mgmt
*   Alternate Payment Services Mgmt

 | 

*   card-payments
*   payment-settlement
*   alternate-payments

 |

2.1.2 Versioning
----------------

Major version only.

![](./1594869768528-Slide1.PNG)

![](./1594869810235-Slide2.PNG)

![](./1594869852648-Slide3.PNG)

![](./1594869889877-Slide4.PNG)

![](./1594869910565-Slide5.PNG)

![](./1594869934709-Slide6.PNG)

![](./1594869954706-Slide7.PNG)

![](./1594869986761-Slide8.PNG)

**Additional References:**

*   **[TTS-5420 Version Management of Information Technology Assets.pdf](https://tmobileusa.sharepoint.com/teams/architecture/Public%20Materials/TTS-5420%20Version%20Management%20of%20Information%20Technology%20Assets.pdf)**
*   **[API Design Guidelines](https://tmobileusa.sharepoint.com/:w:/r/teams/EnterpriseServices/_layouts/15/WopiFrame.aspx?sourcedoc=%7bB647990D-97F1-4ADF-9665-A9E764205973%7d&file=T-Mobile%20API%20Design%20Guidelines%20v1.1-draft.docx&action=default)**

2.1.3 Query Strings
-------------------

Common query strings may be supported by collection resources as shown [here](../../sites/linked/apidesignguidelines/http-method-usage/api-patterns/query-collection)