URI Design
==========

Identifying a Resource
----------------------

According to RFC 3986, the Uniform Resource Identifier (URI) must contain enough information to uniquely identify a resource:

> "An identifier embodies the information required to distinguish what is being identified from all other things within its scope of identification."

This means the API must not combine information from the HTTP headers or the message body with the URI to identify a resource.

_The URI, on its own, does the identifying._

### Examples:

#### Good:

`GET https://dev01.api.t-mobile.com/customer/v1/subscribers/128251151`

returns:

    {
        "firstName": "Julius",
        "lastName": "Caesar",
        "activationDate": "2015-10-14"
    }

If Caesar's internal customerId (128251151) is needed for presentation purposes, then it need not be exposed in the JSON resource representation that is returned.

#### Also Acceptable:

`GET https://dev01.api.t-mobile.com/customer/v1/subscribers/128251151`

returns:

    {
        "firstName": "Julius",
        "lastName": "Caesar",
        "customerId": 128251151,
        "activationDate": "2015-10-14"
    }

Though the customerId appears in both the URI and in the representation, the URI alone is used to locate the resource. The JSON field 'customerId' may be handy for presentation purposes, but it should be treated as an immutable field, same as the activation date. A PUT or PATCH request attempting to change it would be denied with a 403 (Forbidden) status code.

#### Bad:

    POST https://dev01.api.t-mobile.com/customer/v1/getSubscriber
    
    {
        "customerId": 128251151,
    }

returns:

    {
        "firstName": "Julius",
        "lastName": "Caesar",
        "customerId": 128251151,
        "activationDate": "2015-10-14"
    }

As opposed to the earlier examples, the message body includes the identifier. The URI merely identifies something that is more like a gateway lookup resource. This is essentially a Remote Procedure Call and a departure from REST. Note that the URI would look exactly the same for Marc Antony -- not a very useful identifier. (Consider how efficient the U.S. mail system would be if we wrote the same address on the outside of each piece of mail.)

    POST https://dev01.api.t-mobile.com/customer/v1/getSubscriber
    
    {
        "customerId": 117944488,
    }

returns:

    {
        "firstName": "Marcus",
        "lastName": "Antonius",
        "customerId": 117944488,
        "activationDate": "2016-03-15"
    }