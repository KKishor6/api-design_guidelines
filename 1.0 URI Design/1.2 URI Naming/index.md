1.2 URI Naming
==============

1.2.1 Word Choice
-----------------

Resource names should be:

*   pronounceable
*   simple but detailed enough to convey clear meaning
*   hierarchical, rather than descriptive

Avoid:

*   jargon
*   abbreviations (unless there is no reasonable substitute)

#### Examples:

| Good | Bad |
| --- | --- |
| `/customers/157349752/marketing-preferences` | `/customers/157349752/mktg-prefs` |
| `/products?sku=MGAH2LL` | `/products?stock-keeping-unit=MGAH2LL` |
| `/subscribers/{subscriber-id}/mobileNumberChange` | `/subscribers/{subscriber-id}/msisdnChange` |

1.2.2 Hierarchy
---------------

Resource path names should be hierarchical, proceeding from superset to subset as you read from left to right following the package and version number.  
Omit information that does not help to identify the resource.

#### Examples:

| Good | Bad | Comments |
| --- | --- | --- |
| `/states/ga/cities/dunwoody` | `/states/ga/counties/dekalb/cities/dunwoody` | '`counties`' does not help identify the city -- no duplicate city names per state |
| `/customers/19990/` | `/customers/19990/last-name/churchill` | '`/last-name/churchill`' is an attribute and does not help identify customer #19990 |

1.2.3 Case and Valid Characters
-------------------------------

Use lower case letters \[a-z\] and numbers \[0-9\] in resource path names. Use hyphens as word separators as described below. Numbers are fine as identifiers, but begin named resources with a letter.

#### Examples:

| Good | Bad |
| --- | --- |
| `/accounts/1235/alternate-address` | `/customers/19990/2nd-address` |
| `/states/wa/cities/spokane/mayor` | `/states/WA/cities/Sequim/mayor` |

Note: Upper case is valid for a URI according to RFC 3986, but we are limiting our URIs to lower case for the sake of consistency. The path is case-sensitive, while the scheme and the host subcomponent of the URI are not. By limiting ourselves to lower case we avoid any confusion around case mattering in one part of the URI and not the other.

To illustrate, note the relationship between the following URIs:

| # | URI | Comments |
| --- | --- | --- |
| 1 | `https://api.t-mobile.com/billing/v1/subscribers` | good -- all lower case |
| 2 | `https://api.T-Mobile.com/billing/v1/subscribers` | equivalent to #1, despite upper case '`T-Mobile`' |
| 3 | `https://api.t-mobile.com/billing/v1/Subscribers` | not equivalent to #1 or #2 due to upper case 'Subscribers' |

Of the above examples, we would prefer #1 as the others may cause confusion.

1.2.4 Word Separators
---------------------

Use spinal-case: hyphens serve to separate words within a resource name. (Do not use camelCase as it requires the use of capital letters.)

#### Examples:

| Good | Bad | Comments |
| --- | --- | --- |
| `/customers/19990/auto-pay-eligibility` | `/customers/19990/autopayeligibility` | harder to read |
| `/customers/19990/credit-profile` | `/customers/19990/creditProfile` | readable, but we are adhering to lower case for URIs |
| `/customers?bill-type=POSTPAID` | `/customers?billType=POSTPAID` | query string keys should be spinal-case despite mapping to a camel case JSON field name
query string values are whatever case is necessary to correctly represent the value

 |

1.2.5 Nouns
-----------

Nouns should be used for all resource names that correspond to a concrete entity or encapsulate a complex state change to a resource:

#### Examples:

| Good | Bad |
| --- | --- |
| `/customers/{customer-id}/billing-address` | `/customers/{customer-id}/create-billing-address` |
| `/customers/{customer-id}/financial-summary` | `/customers/{customer-id}/get-financial-summary` |
| `/lines-of-service/{line-id}/sim-change-eligibility` | `/lines-of-service/{line-id}/eligible-to-change-sim` |
| `/lines-of-service/{line-id}/sim-change` | `/lines-of-service/{line-id}/change-sim` |
| `/lines-of-service/{line-id}/mobile-number-change` | `/lines-of-service/{line-id}/change-mobile-number` |

1.2.6 Plural Nouns
------------------

A plural noun represents a collection of resources, represented by a JSON array. For example:

    https:api.t-mobile.com/billing/v1/accounts

When the collection name is referenced with an identifier, it returns a single resource:

    GET /accounts/1234

Note that an API is allowed to support a GET on a single resource of a collection without supporting a GET on the entire collection. Resources or sub-resources that are not collections should be singular nouns:

    /employees/12452/company-vehicle/engine

'`employees`' is a collection, but each employee has at most one company-vehicle and each vehicle has a single engine.

It is not always correct to add an 's' to the end of a resource name to make it plural. For example, use

    lines-of-service

not

    line-of-services

#### Examples:

| Good | Bad | Comments |
| --- | --- | --- |
| `/customers/{customer-id}/rate-plan` | `/customers/{customer-id}/rate-plans/{rate-plan-id}` | customer can only have one rate plan at a time, so no need to provide rate-plan-id |
| `/customers/{customer-id}/billing-address` | `/customers/{customer-id}/billing-addresses` | the plural `billing-addresses` implies an array is returned instead of a single object |

1.2.7 Verbs
-----------

> **Warning**

> Use verbs as resources in this exceptional case only. If verbs are the norm, then you may not be designing a REST API.

Almost all resources in a REST API are nouns. They are acted on by the verbs of the HTTP methods. For any resource named with a verb, we can assume that it supports a post-only (RPC-style) call and does not represent an entity or transaction that can be queried or updated later. We can refer to resources with verb names as 'function resources'.

#### Examples:

| URI for Function Resource | Comments |
| --- | --- |
| `/navigation/calculate-distance` | calculates distance between two coordinates |
| `/tax/get-sales-tax-for-location` | takes location and returns effective tax rate |
| `/utilities/encode-string-base64` | takes string and returns it encoded with base64 |
| `/utilities/get-unique-id` | returns a unique ID |
| `/billing/v1/merge-accounts` | takes two accounts and merges them into one |

Note that some of these resources can be forced into noun forms, but do not necessarily convey the purpose of the resource as clearly. In some cases, there is no corresponding noun form for a given verb.