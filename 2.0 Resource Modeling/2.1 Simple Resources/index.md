2.1 Simple Resources
====================

The best way to model a resource for a domain depends on its complexity and behavior. Simple resources can be used to model a domain when it consists of:

*   an immutable entity
*   a mutable entity with simple state changes

Simple Immutable Resources
--------------------------

A simple immutable resource models an entity with fields that cannot be changed. After the initial POST or PUT is used to create the resource, its fields cannot be modified with a POST, PUT or PATCH. Reference data and event histories are commonly modeled this way.

Consider the following resource representing the state of Washington, /states/wa, modeled as a simple object with a single resource:

[![simple resource puml](./simple-resource-puml.png)

For this resource, none of the fields are likely to change. GET would be the main HTTP method used for this resource and PUT and POST would not be supported.

The same would be true for the following event resource describing a promotional discount:

[![event resource puml](./event-resource-puml.png)

The discount is something that occurred in the past, and there is no need to support any modification to it.

Simple Mutable Resources
------------------------

For a slightly different domain capability, the state resource is required to support updates. Adding a single mutable field to an otherwise immutable resource renders it mutable.

[![simple mutable resource puml](./simple-mutable-resource-puml.png)

This change alone would require support for PUT or PATCH to directly update of population. Any attempt to change the value of one of the immutable fields would fail and return a status code of 403 (Forbidden).

Likewise, a resource for a T-Mobile retail store, would be mutable given a representation as follows:

[![mutable retail store puml](./mutable-retail-store-puml.png)

Even if this looks mostly like reference data, and the location and phone number are generally constant, PUT or PATCH would be needed to allow a change in management.

Reference Data
--------------

Although the `/retail-stores` resource is simple, the `storeType` field could be more useful if validated internally against reference data:

*   shoppingCenter
*   kiosk
*   mall
*   freeStanding
*   mobile
*   flagship

When it comes to designing the resource to maintain this reference data, its association to `/retail-stores` could impact its naming. We could choose something like: `/retail-stores/store-types/`.

However, this design could cause difficulty later. There is no hierarchical relationship between the collection of retail stores and the retail store types. These two entities have different lifecycles and will not necessarily remain within the same bounded context. Other resources may also refer to store types.

Instead, it is best to maintain the store-type reference data as a top-level resource with its own URI: `/retail-store-types`

This way other resources will be free to make associations to retail store types without implying that these resources are dependent on the collection of existing retail stores.

> ### **Tip**
> 
> Give reference data its own base URI rather than making it a sub-resource of a resource that happens to reference it.