2.2 Compound Resources
======================

Most domains would be difficult to manage if represented with a single resource. Compound resources are related resources that are either associated or overlapping.

**Associated** resources are related but do not represent duplicate data.

**Overlapping** resources are associated but also have fields in common. The contents of the common or overlapping fields change for both resources when they are updated for one.

The domain may be modeled with compound resources to represent:

*   a subset of fields that are read or updated separately
*   multiple associated entities
*   [multiple views](../../sites/linked/apidesignguidelines/http-method-usage/api-patterns/query-resource) : subsets or supersets of cohesive fields that are read together

Consider a resource representing a vehicle:

[![vehicle resource puml](./vehicle-resource-puml.png)

The first four fields are immutable. The next two fields related to the engine might change, but it would not be a common occurrence.

The 'status' is much more mutable, and it is also read more frequently. For these reasons, we may want to add a separate resource to improve access.

Overlapping Child Resources
---------------------------

Some fields of a resource may have special significance and should be easier to access directly. To isolate a field, a separate resource can be defined:

[![vehicle status resources pumlover](./vehicle-status-resources-pumlover.png)

This resource is _overlapping_ because 'status' is part of both the parent and child vehicle resources.

A GET, PUT or PATCH may be supported for this resource. If PUT or PATCH is supported, any changes made to the resource would naturally be reflected in its parent resource and vice-versa.

Instead of creating a child resource for this one field, PATCH could be used to update the single field of the parent object directly, but PATCH cannot be used to read individual fields of a larger resource.

Multiple Overlapping Fields
---------------------------

A child resource could also be created to break out multiple fields. In the following example, the real-time fields related to location and speed are available as a separate resource:

[![vehicle telemetry resources puml](./vehicle-telemetry-resources-puml.png)

Associated Child Resources
--------------------------

If there is no need for data to be shared between the parent and child, then the resource can be simply associated. There is still a parent-child relationship based on the hierarchy expressed in the URIs, but the content of the child can only be accessed by addressing the child itself.

For example, let's assume that the engine information for the vehicle resource described above is not submitted with the PUT or POST request that creates the resource. Instead it is populated by the server using reference data based on the specified make, model and trim.

The parent resource is created with the following request:

    PUT /vehicles/1B3XP64K0R0019333
    {
      "make": "Dodge",
      "model": "Dart",
      "year": "1976",
      "trim": "Sport",
      "status": "restored"
    }

In addition to creating the parent resource exactly as described, the server can also create an associated child '/engine' resource:

[![associated vehicle resources puml](./associated-vehicle-resources-puml.png)

This allows access to the vehicle's engine information but only when specifically requested and without cluttering the parent vehicle resource.

### Associated Events

Similarly, the server can create a child resource collection and add event resources to record state changes to the parent resource.

For the following customer resource, the parent resource is created with the POST request as follows:

    POST /customers
    {
      "firstName": "Ruppert",
      "lastName": "Jones",
      "birthDate": "1955-03-12"
    }

In response, the server creates the parent resource and a child event resource in an events collection:

[![associated customer resources puml2](./associated-customer-resources-puml2.png)

View Resources
--------------

A view resource is similar to the child resource but with a few differences:

*   it is not strictly a subset but may contain fields the parent does not
*   it is immutable (only supports GET)
*   the string "-view" is appended to the resource name, e.g. `/vehicles/1B3XP64K0R0019333/detail-view`

View resources are useful when different consumers or scenarios require a different scope of detail for the same resource.

Suppose there are three resources for the customer:

*   the parent customer resource
*   a child address resource
*   an event for each customer state change
*   an events collection for accessing the child events

These resources could be represented as follows:

[![customer view resources puml](./customer-view-resources-puml.png)

If all these resources are needed at once for a particular use case, then a view resource can be created that combines the information, such as `/customers/e2792ea4/detail-view` Executing a GET on this resource would return something like the following JSON:

    GET /customers/e2792ea4/detail-view
    {
      "firstName": "Ruppert",
      "lastName": "Jones",
      "birthDate": "1955-03-12",
      "address": {
        "streetAddress": "422 2nd Ave S",
        "city": "Seattle",
        "state": "WA",
        "zip": "98104",
      },
      "events": [
        {
          "eventId": 1,
          "eventType": "customerCreated",
          "eventTime": "2018-05-30T16:46:56"
        },
        {
          "eventId": 2,
          "eventType": "2018-05-30T17:22:03",
          "eventTime": "addressAdded"
        }
      ]
    }

For more information on performing a query on this type of resource, see [here](../../sites/linked/apidesignguidelines/http-method-usage/api-patterns/query-resource)