2.3 Complex Resources
=====================

Both simple and compound resources still assume that it is appropriate for the API consumer to directly manipulate the fields of an entity-style resource for any state transition. However, many state transitions are complex and require:

1.  enforcing dependencies and limitations between fields
2.  re-calculation of some fields based on state changes of other fields
3.  recording additional context information as part of state transitions
4.  supporting access restrictions
    
    *   restricting all changes to a given field
    *   restricting change of a given field to a specific value
5.  tracking each state transition with its own unique identifier for refund or reversal purposes

##### Account Example

Consider a simple account resource:

[![simple account resource puml](./simple-account-resource-puml.png)

Updating directly with a PUT or PATCH seems straightforward for a 'simple' resource like this until typical real-world business rules are taken into account:

When updating these fields, we may be required to:

*   record context (such as reason codes) for status changes
*   allow some status changes only if the balance is zero
*   link balance credits to a unique transaction ID for retries or refunds
*   restrict some balance or status changes to certain users
*   make some status changes terminal
*   allow overrides of terminal status changes for privileged users

##### Enforcing business rules in the parent resource

Attempts to support these business rules by making a PUT request directly to the account resource poses some difficulties:

*   fields must be added to the core account resource to hold required context
*   authorization mechanism must examine message contents
*   dependencies between fields is not easily exposed in Swagger and may be coupled to client code
*   server code lacks any natural boundaries for these rules

### Transaction resources

A **transaction resource** is a child resource that is specifically designed to manage a state change to its parent resource. It is structurally simple and has a simple life cycle in comparison to the parent resource, but it provides:

*   clear documentation of the parent resource's capabilities
*   encapsulation of logic for each type of state change
*   definition of context related to a state change
*   fine grain access control for each type of state change
*   unique identification of state changes for retry purposes

#### Coarse-grained transaction resources

Depending on requirements, transaction resources can be coarse-grained or fine-grained in nature. For the above account resource, it may be sufficient to create a single resource for all changes to the status field:

[![coarse transaction resource puml](./coarse-transaction-resource-puml.png)

For this design, the status could not be changed directly with a request to the account resource. Instead, a request would be made to `/status-change` resource. For example, to cancel the account, the following request would be sent:

    PUT /accounts/1389762/status-change
    {
      "newStatus": "canceled",
      "reason": "poor coverage"
    }

This would result in the status of the resource at /accounts/1389762 changing to 'canceled'. This way the reason field can be required for each status change, but the parent account resource only needs to change the status and has no need to record the reason for the most recent status change:

[![canceled account resource puml](./canceled-account-resource-puml.png)

To change the status back to 'active', the following request would be sent:

    PUT /accounts/1389762/status-change
    {
      "newStatus": "active",
      "reason": "customer error"
    }

#### Fine-grained transaction resources

In many situations, the coarse-grained transaction resource is insufficient:

1.  when required context varies for different status changes
2.  when access to certain status changes must be restricted
3.  when certain changes in status are dependent on the value of other fields

For example, it may be that only users with special privileges can 'reactivate' a canceled account. With a separate URI corresponding to reactivation it is much easier to restrict access to that capability:

[![fine transaction resource puml](./fine-transaction-resource-puml.png)

To cancel the account the following request is sent:

    PUT /accounts/1389762/cancellation
        {
      "reason": "relocation"
    }

To reactivate the account, another request is sent to a separate URI:

    PUT /accounts/1389762/reactivation
       {
      "reason": "relocation canceled"
    }

With this design, it usually makes sense to define a resource corresponding to each valid value of the status field. Although this may cause the creation of several resources, status changes often cover most of the scope of the capability.

#### Uniquely-named transaction resources

Managing state for a field like 'balanceInCents' is more complicated as it is not possible to create predefined child resources corresponding to each value of the field. Instead, we need to define a collection for all the requests that cause the balance to change. The client must add a new uniquely-named resource to this collection for each change to the balance.

No client should be allowed to simply set a monetary balance but only to submit requests to change a balance through a uniquely named child resource. This type of resource should be required for all monetary transactions and for any other transaction for which duplicates are forbidden.

To treat credits and debits separately, two different collections can be supported. For credits, a resource is added to the `/payments` resource with a unique resource name. For debits, the same is done to the `/charges` resource. (For the following diagram, the URIs of the sample payment and charge use abbreviated UUIDs for readability.)

[![uniquely named resources puml](./uniquely-named-resources-puml.png)

A payment is created by submitting a request as follows with a UUID generated by the client:

When the payment or charge is submitted with a UUID generated by the client, the server is free to generate its own corresponding server transaction ID. If the server successfully updates the balance account, it returns a status code of 201 with a location header specifying the same URI as mentioned in the request.

The response may also add a confirmation code or the server transaction ID to the resource and return that in the response body. This depends on requirements.

A sample payment request and response follows:

    PUT https://api.t-mobile.com/accounts/1389762/payments/c520d892-e6a2-4ab0-9320-a000a116dfa9
    Date: Fri, 01 Jun 2018 14:35:26 GMT
    Content-Type: application/json
    {
      "amountInCents": 2500
    }
    HTTP/1.1 201 Created
    Content-Length: 71
    Content-Type: application/json;charset=UTF-8
    Date: Fri, 01 Jun 2018 14:35:27 GMT
    Location: https://api.t-mobile.com/accounts/1389762/payments/c520d892-e6a2-4ab0-9320-a000a116dfa9
    {
      "amountInCents": 2500,
      "confirmationCode": "af6e61c1"
    }

##### Handling transaction resource timeouts

If an attempt to create a transaction resource with a 'PUT' request ends in a timeout, the exact same request can simply be retried. Using PUT, there is no danger of creating a duplicate payment.

When the server receives a retried transaction -- the same URI being sent again -- it must do two things:

1.  verify that the contents of the request are the same
2.  return the same response as if it were seeing the request for the first time

Note that the method of creating a resource can be either a PUT or a POST. If POST is used, there is danger, a timeout will result in a duplicate resource being created. For this reason, PUT is recommended, particularly in the case of financial transactions or those where duplicates might adversely impact customer experience.

For more information, see [PUT vs. POST](../../sites/linked/apidesignguidelines/http-method-usage/put-vs-post) , [Create Resource in Collection](../../sites/linked/apidesignguidelines/http-method-usage/api-patterns/create-resource-in-collection) and [Create Resource by Name](%20https:/devcenter.t-mobile.com/sites/linked/apidesignguidelines/http-method-usage/api-patterns/create-resource-by-name) .

##### Creating a parent resource with a transaction resource

The above examples show how transaction resources can be used to encapsulate _updates_ of the parent resource. However, there may be a need to first encapsulate the _creation_ of the parent resource.

This approach enables:

*   the server to generate the identifier of the parent resource
*   the creation request to be a separate idempotent transaction
*   avoiding sensitive identifiers in the URI

Applying this approach could look like this:

    PUT 
    https://api.t-mobile.com/accounts/create/db8d11ed-561c-469f-98ba-b831ff58e439
    Date: Fri, 01 Jun 2018 19:21:13 GMT
    Content-Type: application/json
    {
      "mobileNumber": "5098675309",
      "ssn": "721-07-4426"
    }
    HTTP/1.1 201 Created
    Content-Length: 112
    Content-Type: application/json;charset=UTF-8
    Date: Fri, 01 Jun 2018 19:21:15 GMT
    Location: https://api.t-mobile.com/accounts/create/db8d11ed-561c-469f-98ba-b831ff58e439
    {
      "accountLink": https://api.t-mobile.com/accounts/213007029,
      "confirmationCode": "73b1f612"
    }

This is preferable to creation requests acting directly on the /accounts resource:

*   `PUT https://api.t-mobile.com/accounts/213007029`
    
    *   does not allow the server to generate the URI for the resource
    *   exposes potentially sensitive identifier in URI
*   `POST https://api.t-mobile.com/accounts`
    
    *   risks duplicate requests