2.0 Resource Modeling
=====================

Design of a REST API is centered on the idea of a **resource**. The structure and granularity of the resource must be carefully modeled to serve as an appropriate abstraction for the capabilities of a domain.

URI, Resource and Representation
--------------------------------

To understand what a resource is and its relationship to its URI and representation, it is helpful to read Roy Fielding's definition of the terms in his [disseration](https://www.ics.uci.edu/~fielding/pubs/dissertation/rest_arch_style.htm)

Resource:

> The key abstraction of information in REST is a resource. Any information that can be named can be a resource: a document or image, a temporal service (e.g. "today's weather in Los Angeles"), a collection of other resources, a non-virtual object (e.g. a person), and so on. In other words, any concept that might be the target of an author's hypertext reference must fit within the definition of a resource.

URI:

> REST uses a resource identifier to identify the particular resource involved in an interaction between components.

Representation:

> REST components perform actions on a resource by using a representation to capture the current or intended state of that resource and transferring that representation between components.

*   **The resource is the abstraction.**
*   **The URI identifies the resource.**
*   **The representation is used to communicate and change the state of the resource.**

Resources instead of Remote Procedure Calls
-------------------------------------------

For those moving to REST from SOAP or other protocols based on remote procedure calls, resource design is likely the greatest adjustment. Note how the REST sequences contrast with the RPC sequence:

**REST, reading a resource:**

For REST reads, we pass an address and get a representation.

[![get sequence puml](./get-sequence_puml.png)

**REST, updating a resource:**

For REST updates, we pass a representation to an address and receive only a status code in response.

[![put sequence puml](./put-sequence_puml.png)

**RPC, any read or update request:**

For all RPC requests, we must design custom request and response payloads.

[![rpc sequence puml](./rpc-sequence_puml.png)

With RPC, there is flexibility (and inconsistency) in the custom payloads that are used for every request and response. Communication is with a server application that must direct the request based on analysis of the payload. The HTTP POST method is used for every request type.

With REST, APIs communicate consistently using the resource representation or no payload at all. Although requests are received by the server, they are routed based on the resource path to individual resource controllers for execution. The resource is paramount to the design because the client is communicating with the resource via its well-defined interface.