4.0 Representation Design
=========================

As stated in the [general guidelines](../../sites/linked/apidesignguidelines/api-documentation), JSON should be supported for all resource representations. For consistency and readability, adhere to the following guidelines for JSON resource representations:

JSON field names should be:

*   camelCase
*   pronounceable
*   simple but detailed enough to convey clear meaning
*   include units for clarity

Avoid:

*   jargon
*   abbreviations (unless there is no reasonable substitute)
*   repetition of an object name in a field name
*   meaningless filler words like 'flag' and 'indicator'

For Swagger-specific documentation standards see [here](../../sites/linked/apidesignguidelines/api-documentation).