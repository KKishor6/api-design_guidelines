compound-resources
==================

[associated-customer-resources.puml](../../sites/linked/apidesignguidelines/all-diagrams/modeling/compound-resources/associated-customer-resources-puml)

[associated-vehicle-resources.puml](../../sites/linked/apidesignguidelines/all-diagrams/modeling/compound-resources/associated-vehicle-resources-puml)

[customer-view-resources.puml](../../sites/linked/apidesignguidelines/all-diagrams/modeling/compound-resources/customer-view-resources-puml)

[vehicle-resource.puml](../../sites/linked/apidesignguidelines/all-diagrams/modeling/compound-resources/vehicle-resource-puml)

[vehicle-status-resources.puml](../../sites/linked/apidesignguidelines/all-diagrams/modeling/compound-resources/vehicle-status-resources-puml)

[vehicle-telemetry-resources.puml](../../sites/linked/apidesignguidelines/all-diagrams/modeling/compound-resources/vehicle-telemetry-resources-puml)