simple-resources
================

[event-resource.puml](../../sites/linked/apidesignguidelines/all-diagrams/modeling/simple-resources/event-resource-puml)

[mutable-retail-store.puml](../../sites/linked/apidesignguidelines/all-diagrams/modeling/simple-resources/mutable-retail-store-puml)

[simple-mutable-resource.puml](%20https:/devcenter.t-mobile.com/sites/linked/apidesignguidelines/all-diagrams/modeling/simple-resources/simple-mutable-resource-puml)

[simple-resource.puml](../../sites/linked/apidesignguidelines/all-diagrams/modeling/simple-resources/simple-resource-puml)