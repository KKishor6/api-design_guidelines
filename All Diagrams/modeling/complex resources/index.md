complex-resources
=================

[canceled-account-resource.puml](../../sites/linked/apidesignguidelines/all-diagrams/modeling/complex-resources/canceled-account-resource-puml)

[coarse-transaction-resource.puml](../../sites/linked/apidesignguidelines/all-diagrams/modeling/complex-resources/coarse-transaction-resource-puml)

[fine-transaction-resource.puml](../../sites/linked/apidesignguidelines/all-diagrams/modeling/complex-resources/fine-transaction-resource-puml)

[simple-account-resource.puml](../../sites/linked/apidesignguidelines/all-diagrams/modeling/complex-resources/simple-account-resource-puml)

[uniquely-named-resources.puml](../../sites/linked/apidesignguidelines/all-diagrams/modeling/complex-resources/uniquely-named-resources-puml)