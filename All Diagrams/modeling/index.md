modeling
========

[complex-resources](../../sites/linked/apidesignguidelines/all-diagrams/modeling/complex-resources)

[compound-resources](../../sites/linked/apidesignguidelines/all-diagrams/modeling/compound-resources)

[get-sequence.puml](../../sites/linked/apidesignguidelines/all-diagrams/modeling/get-sequence-puml)

[put-sequence.puml](../../sites/linked/apidesignguidelines/all-diagrams/modeling/put-sequence-puml)

[rpc-sequence.puml](../../sites/linked/apidesignguidelines/all-diagrams/modeling/rpc-sequence-puml)

[simple-resources](../../sites/linked/apidesignguidelines/all-diagrams/modeling/simple-resources)