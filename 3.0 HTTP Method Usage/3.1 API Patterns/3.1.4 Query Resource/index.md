3.1.4 Query Resource
====================

| Description | Read a single resource |
| --- | --- |
| [**x-api-pattern**](../../sites/linked/apidesignguidelines/api-documentation) | QueryResource |
| **URI Pattern** | `GET /resource`  
`GET /collection/{resource-id}`  
`GET /collection/{resource-id}/sub-resource` |
| **Example URI** | `GET /subscribers/179352144` |
| **Requirements** | safe: does not change state of any resource |
| **Comments** | recommended to return `ETag` for concurrency and cache support  
recommended to support conditional reads using `If-None-Match` header with `ETag`  
`ETag` is preferred over using `Last-Modified`  
`If-None-Match` is preferred over `If-Modified-Since` |
| **Request Body** | none |
| **Response Body** | JSON object (for 200), none for 304 |
| **Success Status Codes** | 200 (OK)  
304 (Not Modified) |

| Request Headers | Use | Example |
| --- | --- | --- |
| Authorization | required | `Authorization: Bearer mF_9.B5f-4.1JqM` |
| If-None-Match | recommended | `If-None-Match: "069dbb7e7b3d7338f3ac7b82da65c22d0"` |
| [activity-id (custom T-Mobile)](../../sites/linked/apidesignguidelines/http-method-usage/headers/custom-headers) | recommended | `activity-id: c34e7acd-384b-4c22-8b02-ba3963682508` |
| Date | optional | `Date: Mon, 05 Mar 2018 16:38:08 GMT` |
| If-Modified-Since | optional | `If-Modified-Since: Mon, 05 Mar 2018 16:38:08 GMT` |
| Accept | optional | `Accept: application/json` |
| Accept-Encoding | optional | `Accept-Encoding: gzip` |
| Accept-Language | optional | `Accept-Language: es` |
| [other custom T-Mobile headers](../../sites/linked/apidesignguidelines/http-method-usage/headers/custom-headers) | optional |   |
| **Response Headers** | **Use** | **Example** |
| Date | required | `Date: Mon, 05 Mar 2018 16:38:15 GMT` |
| Content-Type | required, if content returned | `Content-Type: application/json;charset=UTF-8` |
| Cache-Control | required | `Cache-Control: max-age=31536000` |
| ETag | recommended | `ETag: "069dbb7e7b3d7338f3ac7b82da65c22d0` |
| Content-Length | optional | `Content-Length: 91` |
| Transfer-Encoding | optional | `Transfer-Encoding: chunked` |
| Content-Encoding | optional | `Content-Encoding: gzip` |
| Content-Language | optional | `Content-Language: en` |
| Last-Modified | optional | `Last-Modified: Mon, 05 Mar 2018 16:38:08 GMT` |

| Error Status Codes |
| --- |
| 400 (Bad Request) |
| 401 (Unauthorized) |
| 403 (Forbidden) |
| 404 (Not Found) |
| 405 (Method Not Allowed) |
| 406 (Not Acceptable) |
| 500 (Internal Server Error) |
| 503 (Service Unavailable) |

Query View of Resource
----------------------

In addition, a specific view of resource can be requested. The view or projection is an alternate representation of a resource to suit a particular client need. The URI is equal to the URI of the base resource but includes a view name after the resource identifier.

| Description | Read a specified view of a single resource |
| --- | --- |
| **URI Pattern** | `GET /collection/{resource-id}/alternate-view` |
| **Example URI** | `GET /subscribers/179352144/detail-view`  
`GET /subscribers/179352144/summary-view` |
| **Requirements** | safe: does not change state of any resource  
for the view resource name, follow the convention ‘\[view-name\]-view’: for example:  
\`products/MF053LLA/list-view  
views are designed to be meaningful subsets or supersets of the given resource  
the ETag for a view must be different than that of its base resource |
| **Comments** | recommended to return ETag for concurrency and cache support  
recommended to support conditional reads using `If-None-Match header` with ETag  
ETag is preferred over using `Last-Modified`  
`If-None-Match` is preferred over `If-Modified-Since` |

### Example:

The resource `GET /subscribers/179352144/` may return a representation including the fields:

*   name
*   address
*   balance
*   status

Where a different view of this resource:`GET /subscribers/179352144/subscriber-detail` may return a resource with more information:

*   name
*   address
*   balance
*   status
*   activationDate
*   channelAtActivation
*   eventHistory