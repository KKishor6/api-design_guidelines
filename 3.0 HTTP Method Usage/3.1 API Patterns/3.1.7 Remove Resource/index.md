3.1.7 Remove Resource
=====================

| Description | Idempotently remove a resource |
| --- | --- |
| [**x-api-pattern**](../../sites/linked/apidesignguidelines/api-documentation) | RemoveResource |
| **URI Pattern** | `DELETE /collection/{resource-id}`  
`DELETE /collection/{resource-id}/sub-resource` |
| **Example URI** | `DELETE /customers/1793401/billing-address` |
| **Requirements** | idempotent: repeated requests will succeed even though the resource no longer exists from the client perspective  
a GET request after a successful DELETE returns 404 (Not Found) status code |
| **Comments** | to support idempotency, the server must be able to distinguish between a deleted resource and one that has never existed |
| **Request Body** | none |
| **Response Body** | none |
| **Success Status Codes** | 204 (No Content) |

| Request Headers | Use | Example |
| --- | --- | --- |
| Authorization | required | `Authorization: Bearer mF_9.B5f-4.1JqM` |
| If-Match | recommended | `If-Match: "069dbb7e7b3d7338f3ac7b82da65c22d0"` |
| [activity-id (custom T-Mobile)](../../sites/linked/apidesignguidelines/http-method-usage/headers/custom-headers) | recommended | `activity-id: c34e7acd-384b-4c22-8b02-ba3963682508` |
| Date | optional | `Date: Mon, 05 Mar 2018 16:38:08 GMT` |
| If-Unmodified-Since | optional | `If-Unmodified-Since: Mon, 05 Mar 2018 16:38:08 GMT` |
| [other custom T-Mobile headers](../../sites/linked/apidesignguidelines/http-method-usage/headers/custom-headers) | optional |   |
| **Response Headers** | **Use** | **Example** |
| Date | required | `Date: Mon, 05 Mar 2018 16:38:15 GMT` |

| Error Status Codes |
| --- |
| 400 (Bad Request) |
| 401 (Unauthorized) |
| 403 (Forbidden) |
| 404 (Not Found) |
| 405 (Method Not Allowed) |
| 409 (Conflict) |
| 412 (Precondition Failed) |
| 500 (Internal Server Error) |
| 503 (Service Unavailable) |