3.1.1 Create Resource in Collection
===================================

| Description | Create a new resource in the specified collection and generate a URI for it |
| --- | --- |
| [**x-api-pattern**](../../sites/linked/apidesignguidelines/api-documentation) | CreateInCollection |
| **URI Pattern** | `POST /collection`  
`POST /collection1/{resource-id}/collection2` |
| **Example URI** | `POST /subscribers`  
`POST /customers/{customer-id}/contacts` |
| **Requirements** | ID of new resource is determined by server, not the client |
| **Comments** | **avoid using for financial transactions such as payments**  
non-idempotent: repeated requests (such as in timeout scenarios) may result in duplicate resources  
for idempotent resource creation, use [Create Resource by Name](../../http-method-usage/api-patterns/create-resource-by-name)  
the server may augment the created resource with additional fields not supplied in the request |
| **Request Body** | JSON object representing resource to create |
| **Response Body** | optional, newly created resource if desired |
| **Success Status Codes** | 201 (Created) |
|   | 202 (Accepted) |

| Request Headers | Use | Example |
| --- | --- | --- |
| Authorization | required | `Authorization: Bearer mF_9.B5f-4.1JqM` |
| Content-type | required | `Content-Type: application/json` |
| [activity-id (custom T-Mobile)](../../sites/linked/apidesignguidelines/http-method-usage/headers/custom-headers) | recommended | `activity-id: c34e7acd-384b-4c22-8b02-ba3963682508` |
| Date | optional | `Date: Mon, 05 Mar 2018 16:38:08 GMT` |
| Accept | optional | `Accept: application/json` |
| [other custom T-Mobile headers](../../sites/linked/apidesignguidelines/http-method-usage/headers/custom-headers) | optional |   |
| **Response Headers** | **Use** | **Example** |
| Date | required | `Date: Mon, 05 Mar 2018 16:38:15 GMT` |
| Location | required | `Location: http://api.tmo.com/billing/v1/accounts/33c093e9b05` |
| Content-Type | required, if content returned | `Content-Type: application/json;charset=UTF-8` |
| ETag | recommended | `ETag: "069dbb7e7b3d7338f3ac7b82da65c22d0` |
| Content-Length | optional | `Content-Length: 91` |
| Transfer-Encoding | optional | `Transfer-Encoding: chunked` |

| Error Status Codes |
| --- |
| 400 (Bad Request) |
| 401 (Unauthorized) |
| 403 (Forbidden) |
| 404 (Not Found) |
| 405 (Method Not Allowed) |
| 406 (Not Acceptable) |
| 409 (Conflict) |
| 415 (Unsupported Media Type) |
| 500 (Internal Server Error) |
| 503 (Service Unavailable) |