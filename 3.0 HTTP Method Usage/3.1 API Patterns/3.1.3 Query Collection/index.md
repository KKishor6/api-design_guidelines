3.1.3 Query Collection
======================

| Description | Read the contents of a collection |
| --- | --- |
| [**x-api-pattern**](../../sites/linked/apidesignguidelines/api-documentation) | QueryCollection |
| **URI Pattern** | `GET /collection`  
`GET /collection/{resource-id}/collection` |
| **Example URI** | `GET /product-types` |
| **Requirements** | safe: does not change state of any resource  
returns a JSON array of collection matching search. It could be empty array if no match. |
| **Comments** | 
use pagination query string parameters for large collections

If zero items matches Search, response is 200 OK and empty array.

 |
| **Request Body** | none |
| **Response Body** | JSON array or valid HAL response |
| **Success Status Codes** | 200 (OK) |

| Request Headers | Use | Example |
| --- | --- | --- |
| Authorization | required | `Authorization: Bearer mF_9.B5f-4.1JqM` |
| [activity-id (custom T-Mobile)](../../sites/linked/apidesignguidelines/http-method-usage/headers/custom-headers) | recommended | `activity-id: c34e7acd-384b-4c22-8b02-ba3963682508` |
| Date | optional | `Date: Mon, 05 Mar 2018 16:38:08 GMT` |
| Accept | optional | `Accept: application/json` |
| Accept-Encoding | optional | `Accept-Encoding: gzip` |
| Accept-Language | optional | `Accept-Language: es` |
| [other custom T-Mobile headers](../../sites/linked/apidesignguidelines/http-method-usage/headers/custom-headers) | optional |   |
| **Response Headers** | **Use** | **Example** |
| Date | required | `Date: Mon, 05 Mar 2018 16:38:15 GMT` |
| Content-Type | required, if content returned | `Content-Type: application/json;charset=UTF-8` |
| Content-Length | optional | `Content-Length: 91` |
| Transfer-Encoding | optional | `Transfer-Encoding: chunked` |
| ETag | optional | `ETag: "069dbb7e7b3d7338f3ac7b82da65c22d0"` |
| Cache-Control | optional | `Cache-Control: max-age=31536000` |
| Content-Encoding | optional | `Content-Encoding: gzip` |

| Error Status Codes |
| --- |
| 400 (Bad Request) |
| 401 (Unauthorized) |
| 403 (Forbidden) |
| 404 (Not Found) |
| 405 (Method Not Allowed) |
| 406 (Not Acceptable) |
| 500 (Internal Server Error) |
| 503 (Service Unavailable) |

Query Collection with Query String
----------------------------------

It is a design decision as to which query strings are supported for a collection, but **it is recommended that collections of greater than 100 items support pagination**. Smaller collections need not support any query strings if there is no business requirement.

Note that there may be platform limitations for both the number of items in an array and the response payload size. For example, Apigee on-prem is limited to an array size of 500 and payload size of 3 MB.

| Query Type | Description | Examples |
| --- | --- | --- |
| filtering | return only the resources in the collection whose fields match the specified value | `GET /customer/v1/subscribers?state=wa` |
| paging | return the collection in pages as specified | 
`GET /customer/v1/subscribers?limit=100&page=5`

Note that the pagination block is a sibling block to the actual response payload---Discuss embedded paginations , example v2 “carts”. 

{"pagination": { 

    "page": 5, 

    "totalRecordCount": 606, 

    "totalPages": 7, 

    "currentPageItems": 100, 

    "limit": 100, 

    "current": "https://host:port/`customer/v1/subscribers?limit=100&page=5`", 

    "next": "https://host:port/`customer/v1/subscribers?limit=100&page=6`", 

    "prev": "https://host:port/`customer/v1/subscribers?limit=100&page=4`" 

  },

"subscribers":\[{...}\] 

}

In case of Zero result, HTTP status of 200 OK and a response object like below should be returned:

{"pagination": { 

    "page": 1, 

    "totalRecordCount": 0, 

    "totalPages": 0, 

    "currentPageItems": 0, 

    "limit": 100, 

    "current": "https://host:port/`customer/v1/subscribers?limit=100&page=1`",

    "next": null,

    "previous": null

  },

"subscribers":\[\] 

}

 |
| sorting | return the collection with resources sorted by the field indicated with `sort=fieldname`  
direction indicated to be ascending or descending with `fieldname.dir=asc` or `fieldname.dir=desc` | `GET /customer/v1/subscribers?sort=state&state.dir=desc` |

Querying with Sensitive Information
-----------------------------------

In the case where a query must support [sensitive information](../../sites/linked/apidesignguidelines/uri-design/sensitive-identifiers) as query criteria, the [Execute Function](../../sites/linked/apidesignguidelines/http-method-usage/api-patterns/execute-function) API pattern should be used instead to keep the sensitive information secure.