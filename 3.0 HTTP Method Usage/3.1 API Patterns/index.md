3.1 API Patterns
================

Eight Method and URI combinations address the most common scenarios encountered when designing APIs.

Use the following decision tree to determine which API pattern to follow for a given scenario:

![patterns](./patterns.png "patterns")

Pattern list
------------

Select the associated link for detailed information on how to implement each pattern.

|  # | Scenario | [x-api-pattern](../api-documentation) | Method | URI Path Patterns |
| --- | --- | --- | --- | --- |
| 1 | [Create resource in collection](../../sites/linked/apidesignguidelines/http-method-usage/api-patterns/create-resource-in-collection) | CreateInCollection | POST | `/collection`  
`/collection/{resource-id}/collection` |
| 2 | [Create resource by name](../../sites/linked/apidesignguidelines/http-method-usage/api-patterns/create-resource-by-name) | CreateByName | PUT | `/collection/{resource-id}`  
`/collection/{resource-id}/sub-resource` |
| 3 | [Query collection](../../sites/linked/apidesignguidelines/http-method-usage/api-patterns/query-collection) | QueryCollection | GET | `/collection`  
`/collection/{resource-id}/collection` |
| 4 | [Query resource](../../sites/linked/apidesignguidelines/http-method-usage/api-patterns/query-resource) | QueryResource | GET | `/resource`  
`/collection/{resource-id}`  
`/collection/{resource-id}/sub-resource` |
| 5 | [Replace resource](../../sites/linked/apidesignguidelines/http-method-usage/api-patterns/replace-resource) | ReplaceResource | PUT | `/resource`  
`/collection/{resource-id}`  
`/collection/{resource-id}/sub-resource` |
| 6 | [Modify resource](../../sites/linked/apidesignguidelines/http-method-usage/api-patterns/modify-resource) | ModifyResource | PATCH | `/resource`  
`/collection/{resource-id}`  
`/collection/{resource-id}/sub-resource` |
| 7 | [Remove resource](../../sites/linked/apidesignguidelines/http-method-usage/api-patterns/remove-resource) | RemoveResource | DELETE | `/resource`  
`/collection/{resource-id}`  
`/collection/{resource-id}/sub-resource` |
| 8 | [Execute function](../../sites/linked/apidesignguidelines/http-method-usage/api-patterns/execute-function) | ExecuteFunction | POST | `/function-name` |

Required, recommended and optional headers
------------------------------------------

For each pattern documented above, various HTTP request and response headers are designated to be required, recommended or optional. Based on this designation, a particular header must be documented as follows in the Swagger API definition:

| API Pattern Header Use | Swagger API Header Definition |
| --- | --- |
| required | support with `required: true` |
| recommended | `required: true` is preferred
`required: false` is acceptable

undocumented (not supported) is allowed but not preferred

 |
| optional | depends on API design

`required: true` is acceptable

`required: false` is acceptable

undocumented (not supported) is acceptable

 |