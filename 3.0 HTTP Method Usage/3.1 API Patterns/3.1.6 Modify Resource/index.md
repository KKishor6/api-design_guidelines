3.1.6 Modify Resource
=====================

| Description | Partially update a single resource (not idempotent) |
| --- | --- |
| [**x-api-pattern**](../../sites/linked/apidesignguidelines/api-documentation) | ModifyResource |
| **URI Pattern** | `PATCH /resource`  
`PATCH /collection/{resource-id}`  
`PATCH /collection/{resource-id}/sub-resource` |
| **Example URI** | `PATCH /customers/41592` |
| **Requirements** | update is limited to the fields specified in the request body |
| **Comments** | specify the header `If-Match` with an `ETag` to avoid concurrency issues  
use the `Prefer` header to specify that the full-representation should be returned  
use `Content-Type: application/merge-patch+json` for the request  
the response representation, if any, uses `Content-Type application/json` |
| **Request Body** | a payload conforming to [RFC 7386 JSON Merge Patch](https://tools.ietf.org/html/rfc7386) |
| **Response Body** | default to none, full representation if indicated by `Prefer` header |
| **Success Status Codes** | 200 (OK) |
| 204 (No Content) |   |

| Request Headers | Use | Example |
| --- | --- | --- |
| Authorization | required | `Authorization: Bearer mF_9.B5f-4.1JqM` |
| Content-type | required | `Content-Type: application/json` |
| If-Match | recommended | `If-Match: "069dbb7e7b3d7338f3ac7b82da65c22d0"` |
| [activity-id (custom T-Mobile)](../../sites/linked/apidesignguidelines/http-method-usage/headers/custom-headers) | recommended | `activity-id: c34e7acd-384b-4c22-8b02-ba3963682508` |
| Date | optional | `Date: Mon, 05 Mar 2018 16:38:08 GMT` |
| Prefer | optional | `Prefer: return=representation` |
| If-Unmodified-Since | optional | `If-Unmodified-Since: Mon, 05 Mar 2018 16:38:08 GMT` |
| [other custom T-Mobile headers](../../sites/linked/apidesignguidelines/http-method-usage/headers/custom-headers) | optional |   |
| **Response Headers** | **Use** | **Example** |
| Date | required | `Date: Mon, 05 Mar 2018 16:38:15 GMT` |
| Content-Type | required, if content returned | `Content-Type: application/json;charset=UTF-8` |
| ETag | recommended | `ETag: "069dbb7e7b3d7338f3ac7b82da65c22d0` |
| Content-Length | optional | `Content-Length: 91` |
| Transfer-Encoding | optional | `Transfer-Encoding: chunked` |
| Preference-Applied | optional | `Preference-Applied: return=representation` |
| Last-Modified | optional | `Last-Modified: Mon, 05 Mar 2018 16:38:08 GMT` |

| Error Status Codes |
| --- |
| 400 (Bad Request) |
| 401 (Unauthorized) |
| 403 (Forbidden) |
| 404 (Not Found) |
| 405 (Method Not Allowed) |
| 409 (Conflict) |
| 412 (Precondition Failed) |
| 415 (Unsupported Media Type) |
| 500 (Internal Server Error) |
| 503 (Service Unavailable) |