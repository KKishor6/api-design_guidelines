3.1.2 Create Resource by Name
=============================

| Description | Idempotently create a single resource for the specified URI |
| --- | --- |
| [**x-api-pattern**](../../sites/linked/apidesignguidelines/api-documentation) | CreateByName |
| **URI Pattern** | `PUT /collection/{resource-id}`  
`PUT /collection/{resource-id}/sub-resource` |
| **Example URI** | `PUT /payments/58ef5f96-c4c8-11e7-abc4-cec278b6b50a`  
`PUT /customers/{customer-id}/billing-address`  
`PUT /account/{account-id}/suspension` |
| **Requirements** | creates a new resource with the designated URI  
idempotent: repeated requests will succeed without creating additional resources  
client is responsible for generating unique ID or obtaining one from the server before making request |
| **Comments** | specify the header `If-None-Match=*` to make the create explicit and force failure if the resource already exists  
the server may augment the created resource with additional fields not supplied in the request |
| **Request Body** | JSON object representing resource to create |
| **Response Body** | optional, newly created resource if desired |
| **Success Status Codes** | 201 (Created) |
|   | 202 (Accepted) |

| Request Headers | Use | Example |
| --- | --- | --- |
| Authorization | required | `Authorization: Bearer mF_9.B5f-4.1JqM` |
| Content-type | required | `Content-Type: application/json` |
| [activity-id (custom T-Mobile)](../../sites/linked/apidesignguidelines/http-method-usage/headers/custom-headers) | recommended | `activity-id: c34e7acd-384b-4c22-8b02-ba3963682508` |
| Date | optional | `Date: Mon, 05 Mar 2018 16:38:08 GMT` |
| If-None-Match | optional | `If-None-Match=*` |
| Accept | optional | `Accept: application/json` |
| [other custom T-Mobile headers](../../sites/linked/apidesignguidelines/http-method-usage/headers/custom-headers) | optional |   |
| **Response Headers** | **Use** | **Example** |
| Date | required | `Date: Mon, 05 Mar 2018 16:38:15 GMT` |
| Location | required | `Location: http://api.tmo.com/billing/v1/accounts/33c093e9b05` |
| Content-Type | required, if content returned | `Content-Type: application/json;charset=UTF-8` |
| ETag | recommended | `ETag: "069dbb7e7b3d7338f3ac7b82da65c22d0` |
| Content-Length | optional | `Content-Length: 91` |
| Transfer-Encoding | optional | `Transfer-Encoding: chunked` |

| Error Status Codes |
| --- |
| 400 (Bad Request) |
| 401 (Unauthorized) |
| 403 (Forbidden) |
| 404 (Not Found) |
| 405 (Method Not Allowed) |
| 406 (Not Acceptable) |
| 409 (Conflict) |
| 412 (Precondition Failed) |
| 415 (Unsupported Media Type) |
| 500 (Internal Server Error) |
| 503 (Service Unavailable) |