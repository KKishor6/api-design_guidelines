3.1.8 Execute Function
======================

| Description | Execute Function |
| --- | --- |
| [**x-api-pattern**](../../sites/linked/apidesignguidelines/api-documentation) | ExecuteFunction |
| **URI Pattern** | `POST /function-name`  
`POST /collection/function-name` |
| **Example URI** | `POST /calculate-sales-tax`  
`POST /accounts/search` |
| **Requirements** | Use in exceptional cases only  
No resource is created by this request |
| **Comments** | The resource is designed as needed to receive inputs and return outputs |
| **Request Body** | optional, JSON object representing function inputs |
| **Response Body** | optional, JSON object representing function outputs |
| **Success Status Codes** | 200 (OK) |
|   | 202 (Accepted) |
|   | 204 (No Content) |
|   | 207 (Multi-Status) |

| Request Headers | Use | Example |
| --- | --- | --- |
| Authorization | required | `Authorization: Bearer mF_9.B5f-4.1JqM` |
| Content-type | required, if request body included | `Content-Type: application/json` |
| [activity-id (custom T-Mobile)](../../sites/linked/apidesignguidelines/http-method-usage/headers/custom-headers) | recommended | `activity-id: c34e7acd-384b-4c22-8b02-ba3963682508` |
| Date | optional | `Date: Mon, 05 Mar 2018 16:38:08 GMT` |
| Accept | optional | `Accept: application/json` |
| Accept-Encoding | optional | `Accept-Encoding: gzip` |
| Accept-Language | optional | `Accept-Language: es` |
| [other custom T-Mobile headers](../../sites/linked/apidesignguidelines/http-method-usage/headers/custom-headers) | optional |   |
| **Response Headers** | **Use** | **Example** |
| Date | required | `Date: Mon, 05 Mar 2018 16:38:15 GMT` |
| Content-Type | required, if content returned | `Content-Type: application/json;charset=UTF-8` |
| Content-Length | optional | `Content-Length: 91` |
| Transfer-Encoding | optional | `Transfer-Encoding: chunked` |
| Content-Encoding | optional | `Content-Encoding: gzip` |

| Error Status Codes |
| --- |
| 400 (Bad Request) |
| 401 (Unauthorized) |
| 403 (Forbidden) |
| 404 (Not Found) |
| 405 (Method Not Allowed) |
| 409 (Conflict) |
| 415 (Unsupported Media Type) |
| 500 (Internal Server Error) |
| 503 (Service Unavailable) |

Custom Query Functions
----------------------

The Execute Function pattern may be used to perform a query, particularly in the case where the query criteria may consist of [sensitive information](../../sites/linked/apidesignguidelines/uri-design/sensitive-identifiers).

If the API call returns a JSON array of resources, it may be desirable to support pagination and possibly sorting. Even though the URI being invoked actually addresses a 'search' resource, the API can use the same query string syntax as described [here](../../sites/linked/apidesignguidelines/http-method-usage/api-patterns/query-collection) .

For example, we could ask for page 3 of search results though the search criteria itself is passed in the request body rather than in the URI:

`POST /subscribers/search?limit=100&page=3`

This way we avoid designing new syntax for pagination.