3.1.5 Replace Resource
======================

| Description | Idempotently replace a single resource at the specified URI |
| --- | --- |
| **[x-api-pattern](../../sites/linked/apidesignguidelines/api-documentation)** | ReplaceResource |
| **URI Pattern** | `PUT /collection/{resource-id}` |
| **Example URI** | `PUT /subscriber/98108127/billing-address` |
| **Requirements** | the entire resource represenation must be submitted with the request  
idempotent: repeated requests will succeed without generating additional domain change events  
if retry for timeout is occurring following a competing update, `ETag` must be used to resolve  
if the requested change is not valid for the current state of the resource the request must fail with a 409 status code |
| **Comments** | it is recommended to support the header `If-Match` with an `ETag` to avoid concurrency issues  
if `ETag` is returned in the response, it can only be returned when the response representation is an exact match of request representation (see [RFC 7231](https://tools.ietf.org/html/rfc7231#section-7.2))  
the same condition holds true if returning the Last-Modified header in the response |
| **Request Body** | JSON object representing resource to replace |
| **Response Body** | none |
| **Success Status Codes** | 204 (No Content) |

| Request Headers | Use | Example |
| --- | --- | --- |
| Authorization | required | `Authorization: Bearer mF_9.B5f-4.1JqM` |
| Content-type | required | `Content-Type: application/json` |
| If-Match | recommended | `If-Match: "069dbb7e7b3d7338f3ac7b82da65c22d0"` |
| [activity-id (custom T-Mobile)](../../sites/linked/apidesignguidelines/http-method-usage/headers/custom-headers) | recommended | `activity-id: c34e7acd-384b-4c22-8b02-ba3963682508` |
| Date | optional | `Date: Mon, 05 Mar 2018 16:38:08 GMT` |
| If-Unmodified-Since | optional | `If-Unmodified-Since: Mon, 05 Mar 2018 16:38:08 GMT` |
| [other custom T-Mobile headers](../../sites/linked/apidesignguidelines/http-method-usage/headers/custom-headers) | optional |   |
| **Response Headers** | **Use** | **Example** |
| Date | required | `Date: Mon, 05 Mar 2018 16:38:15 GMT` |
| Content-Type | required, if content returned | `Content-Type: application/json;charset=UTF-8` |
| ETag | recommended | `ETag: "069dbb7e7b3d7338f3ac7b82da65c22d0` |
| Content-Length | optional | `Content-Length: 91` |
| Transfer-Encoding | optional | `Transfer-Encoding: chunked` |
| Last-Modified | optional | `Last-Modified: Mon, 05 Mar 2018 16:38:08 GMT` |

| Error Status Codes |
| --- |
| 400 (Bad Request) |
| 401 (Unauthorized) |
| 403 (Forbidden) |
| 404 (Not Found) |
| 405 (Method Not Allowed) |
| 409 (Conflict) |
| 412 (Precondition Failed) |
| 415 (Unsupported Media Type) |
| 500 (Internal Server Error) |
| 503 (Service Unavailable) |