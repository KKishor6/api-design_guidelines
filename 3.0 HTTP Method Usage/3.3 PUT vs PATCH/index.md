[](#33-put-vs-patch)3.3 PUT vs. PATCH
=====================================

Head-to-head Comparison:

| Method | Uses | Request body | Content-Type | Idempotent | Advantage |
| --- | --- | --- | --- | --- | --- |
| [PUT](https://tools.ietf.org/html/rfc7231#section-4.3.4) | 
1.  create
2.  full replace

 | full resource representation; ‘partial PUT’ not allowed | `application/json` | yes | idempotent for create and replace |
| [PATCH](https://tools.ietf.org/html/rfc5789) | partial update | diff (instructions to modify using [RFC 7386 JSON Merge Patch](https://tools.ietf.org/html/rfc7386)) | `application/merge-patch+json` | no | smaller payload  
no need to define sub-resources for field isolation |

To understand the differences between PUT and PATCH, it helps to consult [RFC 7231](https://tools.ietf.org/html/rfc7231#section-4.3.4) which defines PUT:

> Partial content updates are possible by targeting a separately identified resource with state that overlaps a portion of the larger resource, or by using a different method that has been specifically defined for partial updates for example, the PATCH method defined in [RFC5789](https://tools.ietf.org/html/rfc5789).

So according to the RFC, there are two valid options for partially changing a resource:

1.  break the resource into smaller sub-resources and update the sub-resource with a PUT
2.  use PATCH directly on the parent resource

**Note that partially updating a resource using PUT is not allowed:**

> The PUT method requests that the state of the target resource be created or replaced with the state defined by the representation enclosed in the request message payload. A successful PUT of a given representation would suggest that a subsequent GET on that same target resource will result in an equivalent representation being sent in a 200 (OK) response.

\--[RFC 7231](https://tools.ietf.org/html/rfc7231#section-4.3.4)

> The PUT method is already defined to overwrite a resource with a complete new body, and cannot be reused to do partial changes.

\--[RFC 5789](https://tools.ietf.org/html/rfc5789#section-1)

[](#examples)Examples:
----------------------

Consider an account resource with a representation as follows:

    GET /account/1 HTTP/1.1
    accept: application/json; charset=utf-8
    {
    "firstName": "Julius",  
    "lastName": "Caesar",  
    "contactMethod": "email",  
    "emailAddress": "caesar@[rome.gov](http://rome.gov)"  
    }

We can update the resource with a PUT by submitting the entire resource representation with a new contactMethod:

    PUT /account/1 HTTP/1.1
    content-type: application/json; charset=utf-8
    {
    "firstName": "Julius",
    "contactMethod": "none",
    "emailAddress": "caesar@[rome.gov](http://rome.gov/)"  
    }

We can also update with a PATCH by submitting only the new contactMethod:

    PATCH /target HTTP/1.1
    content-type: application/merge-patch+json
    {
    "contactMethod": "none"
    }

In this example, there is not much advantage to using PATCH, but for a resource with many fields, coding can be simplified and payload size reduced.