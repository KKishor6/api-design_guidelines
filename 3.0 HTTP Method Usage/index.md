3.0 HTTP Method Usage
=====================

The URI identifies a resource, but the supported HTTP methods for a resource tell us what we can do with it.

Each method is clearly defined according to the HTTP specification, and it is the responsibility of developers that each method behaves correctly.  The methods are summarized as follows, (see method name links for HTTP specs).

| Method | Uses | Valid Success Codes | Safe | Idempotent | Request Body | Response Body |
| --- | --- | --- | --- | --- | --- | --- |
| [POST](https://tools.ietf.org/html/rfc7231#section-4.3.3) | 1\. Non-idempotent create
2\. Non-idempotent update

3\. Whatever you want

 | 200 (Ok)  
201 (Created)  
202 (Accepted)  
303 (See Other) | no | no | varies | varies |
| [GET](https://tools.ietf.org/html/rfc7231#section-4.3.1) | Query resource | 200 (Ok)  
304 (Not Modified) | yes | yes | none | representation of resource |
| [PUT](https://tools.ietf.org/html/rfc7231#section-4.3.4) | Idempotent create  
Idempotent replace | 201 (Created)  
202 (Accepted)  
204 (No content) | no | yes | representation of resource | varies |
| [PATCH](https://tools.ietf.org/html/rfc5789) | Partial update | 200 (Ok)  
204 (No content) | no | no | [JSON Merge Patch](https://tools.ietf.org/html/rfc7386) | varies |
| [DELETE](https://tools.ietf.org/html/rfc7231#section-4.3.5) | Delete resource | 204 (No content) | no | yes | none | none |
| [OPTIONS](https://tools.ietf.org/html/rfc7231#section-4.3.7) | Get supported methods and CORS information | 200 (Ok) | yes | yes | none | none |