3.4 Headers
===========

This page summarizes HTTP header information.  See the RFCs for a detailed description of all headers.

Most headers are described in [RFC7231](https://tools.ietf.org/html/rfc7231#section-5).

Conditional headers are described in [RFC7232](https://tools.ietf.org/html/rfc7232#section-3).

Cache control headers are described in [RFC7234](https://tools.ietf.org/html/rfc7234#section-5).

The Prefer header is described in [RFC7240](https://tools.ietf.org/html/rfc7240).

The Content-Length header is described in [RFC7230](https://tools.ietf.org/html/rfc7230#section-3.3.2).

Request Headers:
----------------

| Header | Description | Values | Use | Example | Related Status Codes |
| --- | --- | --- | --- | --- | --- |
| Authorization | authorization header for the request | String, see [RFC 6750](https://tools.ietf.org/html/rfc6750) | required | `Authorization: Bearer mF_9.B5f-4.1JqM` | 401 (Unauthorized) |
| Date | date and time of the request based on the client’s clock | [RFC 5322](https://tools.ietf.org/html/rfc5322#section-3.3) Date | recommended | `Date: Mon, 05 Mar 2018 16:38:08 GMT` |   |
| Content-Type | content-type of request body if exists, application/json expected, except for PATCH | application/json,  
application/merge-patch+json | required (if request body exists) | `Content-Type: application/json; charset=UTF-8` | 415 (Unsupported Media Type) |
| Accept | preferred content type – default to application/json | application/json | optional | `Accept: application/json` | 406 (Not Acceptable) |
| Accept-Encoding | preferred encoding of response body for better performance | gzip, deflate | optional | `Accept-Encoding: gzip` |   |
| Accept-Language | list of natural languages preferred in response | en, es | optional | `Accept-Language: en`e |   |
| Content-Length | length of the request body in octets | number | optional | `Content-Length: 91` | 411 (Length Required) |
| If-None-Match | for conditional GET: success if representation exists with specified ETag | ETag | recommended | `If-None-Match: "069dbb7e7b3d7338f3ac7b82da65c22d0"` | 304 (Not Modified) |
| If-Modified-Since | for conditional GET:  
success if representation exists with more recent date than specified | [RFC 5322](https://tools.ietf.org/html/rfc5322#section-3.3) Date | optional | `If-Modified-Since: Mon, 05 Mar 2018 16:38:08 GMT` | 304 (Not Modified) |
| If-Unmodified-Since | for conditional POST, PUT, PATCH, DELETE:  
success if no representation exists with more recent date than specified | [RFC 5322](https://tools.ietf.org/html/rfc5322#section-3.3) Date | optional | `If-Unmodified-Since: Mon, 05 Mar 2018 16:38:08 GMT` | 412 (Precondition Failed) |
| Prefer | whether to return an empty response body after a create or update | return=minimal,  
return=representation  
[see RFC 7240](https://tools.ietf.org/html/rfc7240) | optional | `Prefer: return=representation` | 204 (No Content) |
| Origin | for simple CORS requests, the host from which the request originated | host with scheme, domain and port | optional | `Origin: https://www.t-mobile.com` |   |
| Host | for simple CORS requests, the host to which the request was sent | host name | optional | `Host: api.t-mobile.com` |   |
| Access-Control-Request-Method | for CORS pre-flight OPTIONS requests sent by a browser, method to be used | method type | optional | `Access-Control-Request-Method: POST` |   |
| Access-Control-Request-Headers | for CORS pre-flight OPTIONS requests sent by a browser, headers to be used | headers | optional | `Access-Control-Request-Headers: Authorization, Content-Type` |   |

Response Headers:
-----------------

### Are response headers returned only with success?

Response headers are generally returned with success/2xx status codes and cannot be expected for non-2xx responses, but there are exceptions:

*   **304, Not Modified** - According to [RFC 7232, section 4.1](https://tools.ietf.org/html/rfc7232#section-4.1):

> The server generating a 304 response MUST generate any of the following header fields that would have been sent in a 200 (OK) response to the same request: Cache-Control, Content-Location, Date, ETag, Expires, and Vary.

*   **Other 3xx Responses** - Several redirect status codes return the Location header to indicate the new URI of a resource.
*   **405, Method Not Allowed** - According to [RFC 7232, section 6.5.5](https://tools.ietf.org/html/rfc7231#section-6.5.5):

> The origin server MUST generate an Allow header field in a 405 response containing a list of the target resource's currently supported methods.

| Header | Description | Values | Use | Example |
| --- | --- | --- | --- | --- |
| Date | date and time of the request based on the server’s clock | [RFC 5322](https://tools.ietf.org/html/rfc5322#section-3.3) Date | required | `Date: Mon, 05 Mar 2018 16:38:08 GMT` |
| Content-Type | content-type of response body; `application/json` is expected but hypermedia variants like HAL are also possible | application/json,  
application/hal+json | required (if response body exists) | `Content-Type: application/json; charset=UTF-8` |
| Content-Length | length of the response body in octets; not supported if Transfer-Endoding = chunked | number | optional | `Content-Length: 91` |
| Content-Language | language of response content, defaults to en-US | en, es | optional | `Content-Language: en-US` |
| Content-Encoding | indicates what content encodings have been applied to the representation | gzip, deflate | optional | `Content-Encoding: gzip` |
| Transfer-Encoding | indicates that data is streamed as a series of chunks with the final zero-length chunk indicating completion | chunked | optional | `Transfer-Encoding: chunked` |
| Preference-Applied | indicates that the Prefer header specified in the request was successfully applied | return=minimal,  
return=representation | optional, if Prefer header in request | `Preference-Applied: return=minimal` |
| ETag | entity tag: the version of the resource, usually a hash of the representation | string | recommended | `ETag: "069dbb7e7b3d7338f3ac7b82da65c22d0"` |
| Location | the URI of the newly created resource | URI | required for created resources | `Location: http://api.tmo.com`  
`/billing/v1/accounts/33c093e9b05` |
| Last-Modified | the last modification date of the resource | [RFC 5322](https://tools.ietf.org/html/rfc5322#section-3.3) Date | optional | `Last-Modified: Mon, 05 Mar 2018 16:38:08 GMT` |
| Cache-Control | cache policy supported for the resource by the server:  
`max-age` is number of seconds until stale.  
`no-cache` means ETag must be verified before cache is used.  
`no-store` means the client is not allowed to cache the representation.  
If possible, it is preferable to use max-age with as low a value as necessary. | max-age=x  
no-cache  
no-store | recommended | `Cache-Control: max-age=31536000` |
| Allow | list of supported methods for URI | HTTP method names | required with 405 response | `Allow: GET,HEAD,POST` |
| Access-Control-Allow-Origin | for CORS, an allowed origin, used by the browser to block disallowed requests | host with scheme, domain and port | optional | `Access-Control-Allow-Origin: https://www.t-mobile.com` |
| Access-Control-Allow-Methods | for CORS pre-flight OPTIONS requests, used by the browser to block disallowed requests | comma-separated list of methods | optional | `Access-Control-Allow-Methods: POST, PUT, GET. OPTIONS` |
| Access-Control-Allow-Headers | for CORS pre-flight OPTIONS requests, used by the browser to block disallowed requests | comma-separated list of headers | optional | `Access-Control-Allow-Headers: Authorization, Content-Type` |