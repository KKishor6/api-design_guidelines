3.5 Status Codes
================

The following information is summarized from [RFC7231](https://tools.ietf.org/html/rfc7231#section-6), see the RFC for a detailed description of all status codes.

The general meaning of HTTP status codes are classified based on their first digit as follows:

| Type | Code | Error message returned | Description |
| --- | --- | --- | --- |
| Success | 2xx | no | request received, understood and accepted |
| Multiple choices | 3xx | no | target resource has more than one representation, each with its own identifier; client may want to redirect |
| Client error | 4xx | yes | client has erred as described in response message |
| Server error | 5xx | yes | server has erred as described in response message |

The Response Payload for 200 success status code
------------------------------------------------

Status code 200 always indicates success, but the associated response differs depending upon the method:

| Method | Response payload for status code 200 |
| --- | --- |
| GET | a representation of the resource, cacheable by default unless indicated otherwise by cache controls |
| POST | a representation of the status of, or results obtained from, the action |
| PUT | a representation of the replaced resource if the Prefer request header is specified: `Prefer: return=representation` |
| PATCH | a representation of the patched resource if the Prefer request header is specified: `Prefer: return=representation` |
| OPTIONS | no payload  
returns `Allow` header listing methods for the specified URI  
returns Access Control headers for CORS pre-flight requests |

Common Success & Error Codes
----------------------------

| Series | Code | Description | HTTP Operation | API Pattern | Response Body | Where to Implement |
| --- | --- | --- | --- | --- | --- | --- |
| 2xx | 200 Success | No error, operation successful. If you return no content, return a 204 instead. | GET, PUT, PATCH, POST | QueryCollection, QueryResource, CreateByName, ReplaceResource, ExecuteFunction | Resource - refer above table | uService/Service Layer |
| \- | 201 Created | Successful creation of a resource. The Location HTTP Header provides a link to the newly created resource. | POST | CreateInCollection | resource created, URI returned in location header | uService/Service Layer |
| \- | 202 Accepted | The request has been accepted for further processing. Used for `Asynchronous Operations` and in `Command APIs` to allow a consumer to initiate an asynchronous request. | GET, POST, PUT, DELETE, PATCH | QueryCollection, QueryResource, CreateInCollection, CreateByName, ReplaceResource, RemoveResource | request accepted, return Asynchronous promise | uService/Service Layer |
| \- | 204 No Content | The request was processed successfully, but no response body is returned. | POST, PUT, PATCH, DELETE | ExecuteFunction,ReplaceResource. RemoveResource, ModifyResource | Empty | uService/Service Layer |
| \- | 207 Multi-Status | The request was processed partially, some resource/sub-resource sections had errors. Consider using “Warning” : 112 - “network down” response Header with HTTP status code 200, instead of 207. | POST | ExecuteFunction | resource with processing status(s) as appropriate | uService/Service Layer |
| 3xx | 304 Not Modified | Resource has not been modified. Used when HTTP caching headers are in play. | conditional GET | QueryCollection, QueryResource | Empty | Gateway or Caching Layer |
| \- | 308 Permanent Redirect | Resource has been permanently moved to another URL. Used when a resource ID has been permanently changed and the consumer should be aware of this change, perhaps due to a business process or structure change or canonicalization of a non-standard part number. Should also return a Location Header | GET, POST, PUT, DELETE, PATCH | QueryCollection, QueryResource, CreateInCollection, CreateByName, ReplaceResource, RemoveResource, ModifyResource, ExecuteFunction | Empty | Gateway or uService/Service Layer |
| 4xx | 400 Bad Request | Malformed syntax, bad query, or the request is not valid for the current state of the resource. | GET, POST, PUT, DELETE, PATCH | QueryCollection, QueryResource, CreateInCollection, CreateByName, ReplaceResource, RemoveResource, ModifyResource, ExecuteFunction | Error Message | Gateway and uService/Service Layer |
| \- | 401 Unauthorized | Action requires user authentication and authentication headers were not provided, or authentication failed. | GET, POST, PUT, DELETE, PATCH | QueryCollection, QueryResource, CreateInCollection, CreateByName, ReplaceResource, RemoveResource, ModifyResource, ExecuteFunction | Error Message | Gateway and/or uService/Service Layer - |
| \- | 403 Forbidden | When authentication succeeded but authenticated user doesn't have access to the resource. | GET, POST, PUT, DELETE, PATCH | CreateInCollection, CreateByName, QueryCollection, QueryResource,  ReplaceResource,ModifyResource, RemoveResource,ExecuteFunction | Error Message | Gateway and/or uService/Service Layer |
| \- | 404 Not Found | Resource not found. The endpoint will return this status if there is an endpoint at the URL; if not, the gateway returns this status. | GET, POST, PUT, DELETE, PATCH | QueryCollection, QueryResource, CreateInCollection, CreateByName, ReplaceResource, RemoveResource, ModifyResource, ExecuteFunction | Error Message | Gateway or uService/Service Layer |
| \- | 405 Method Not Allowed | Method not allowed on resource. This decision is a fixed component of the endpoint; for instance, if the endpoint is read-only. If a lack of authorization prevents the action, 403 should be returned instead. | GET, POST, PUT, DELETE, PATCH | QueryCollection, QueryResource, CreateInCollection, CreateByName, ReplaceResource, RemoveResource, ModifyResource, ExecuteFunction | Error Message | Gateway |
| \- | 406 Not Acceptable | Requested representation not available for the resource. Use this when the requested format in the Accept header is not available. | GET | QueryCollection, QueryResource | Error Message | Gateway |
| \- | 409 Conflict | The change cannot be made to the resource in its current state | POST, PUT, DELETE, PATCH | CreateInCollection, ExecuteFunction, CreateByName, ReplaceResource, ModifyResource | Error Message | uService/Service Layer |
| \- | 410 Gone | Indicates that the resource at this end point is no longer available. Useful as a blanket response for old API versions. | GET, PUT, PATCH | QueryCollection, QueryResource, CreateByName, ReplaceResource, RemoveResource, ModifyResource | Error Message | Gateway |
| \- | 412 Precondition failed | One or more conditions given in the request header fields evaluated to false when tested on the server | GET, POST, PUT, DELETE, PATCH | QueryCollection, QueryResource, CreateInCollection, CreateByName, ReplaceResource, RemoveResource, ModifyResource | Error Message | Gateway |
| \- | 415 Unsupported Media Type | The media type of the representation in the request is not supported by the service. | POST, PUT, DELETE, PATCH | CreateInCollection, ExecuteFunction, CreateByName, ReplaceResource, ModifyResource | Error Message | Gateway |
| \- | 429 Too Many Requests | Used to enforce rate limiting or throttling policies. Should return a Retry-After header. | GET, POST, PUT, DELETE, PATCH | QueryCollection, QueryResource, CreateInCollection, CreateByName, ReplaceResource, RemoveResource, ModifyResource | Reference to rate limiting policy | Gateway |
| 5xx | 500 Server Error | Internal server error. While this error is likely generated by the endpoint, the Gateway is responsible for determining whether to return the 500 or some other status code. For instance, some 500 errors at the endpoint may be provoked by an invalid request, which would require the Gateway to respond with a 400 status code. | GET, POST, PUT, DELETE, PATCH | QueryCollection, QueryResource, CreateInCollection, CreateByName, ReplaceResource, RemoveResource, ModifyResource | Error Message | Gateway |
| \- | 503 Service Unavailable | Should provide a Retry-After Header | GET, POST, PUT, DELETE, PATCH | QueryCollection, QueryResource, CreateInCollection, CreateByName, ReplaceResource, RemoveResource, ModifyResource | Error Message | uService/Service Layer or Gateway |

Error Response Format
---------------------

Both the 4xx and 5xx status codes require that the server return a response message describing the error.  The format of the response is described [here](../http-method-usage/status-codes/error-response-format).