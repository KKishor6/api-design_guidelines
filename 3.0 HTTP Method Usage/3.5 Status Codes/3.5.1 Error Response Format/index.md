3.5.1 Error Response Format
===========================

Response accompanied by a status code in the 4xx-5xx range must include a response message body in JSON that describes the error in detail.

The status code carries more weight than the response message as the status code is the only standard part of the error response and provides the overall reason for failure.

The status code is the only numeric code returned for the response. Defining any additional set of numeric codes should be avoided as they would not be self-documenting, and there is no need to establish another set of numeric codes that spans the enterprise or even a particular domain.

Format the error response as follows:

| field | format | description | required/optional |
| --- | --- | --- | --- |
| code | String | succinct, domain-specific, human-readable text string to identify the type of error for the given status code | required |
| userMessage | String | human readable text that describes the nature of the error | required |
| systemMessage | String | text that provides a more detailed technical explanation of the error | optional |
| detailLink | String, URI | link to custom information providing greater detail on error or errors | optional |

#### code

In most cases the status code is sufficient to determine next steps for the API consumer. In some cases, however, the client may need to further evaluate the error response.  For example, if the API required both an identifier for a product and an offer, the response can return different codes that correspond to 404 Not Found, such as ProductNotFound and OfferNotFound. This would allow the client to better understand what went wrong.

#### userMessage

The human-readable text describing the error. Some clients may choose to display this text directly to the user, so it should only contain information that has clear meaning to a user.

#### systemMessage

The systemMessage is optional but may be used to return information that allows the API consumer to better understand the error.

> **warning**
> 
> The systemMessage must not include anything, such as a stack trace, that could potentially reveal information used to exploit the system.

#### detailLink

It is possible that more than one thing went wrong, and the server is sophisticated enough to provide more information. If supported, additional information may be returned via a separate link in the `detailLink` field.

The resource located at `detailLink` may be specific to this request or apply to this domain generally. It may vary in content type and structure to suit the application. **This is an optional field and, for the sake of simplicity, should not be returned in most cases.**

#### Example Error Response

The following is an example error response with all optional fields included:

    HTTP/1.1 404 Not Found
    Content-Length: 231
    Content-Type: application/json;charset=UTF-8
    Date: Wed, 27 Dec 2017 20:49:21 GMT
    {
        "code": "ProductNotFound",
        "userMessage": "Product with the SKU 80275D could not be found",
        "systemMessage": "404 Not Found"
        "detailLink" : "https://api.t-mobile.com/order-managment-errors/21b7c4d3"
    }