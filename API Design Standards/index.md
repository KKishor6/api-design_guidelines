API Design Standards
====================

| Working Group | Slack Channel |
| --- | --- |
| Shawn Schantz | #da-standards |
| Himanshu Kumar | #api-champions |

Definition of an API
--------------------

### Acronym **"API"** stands for **A**pplication **P**rogramming **I**nterface

NIST’s definition of an [API](https://csrc.nist.gov/glossary/term/Application_Programming_Interface): A system access point or library function that has a well-defined syntax and is accessible from application programs or user code to provide well-defined functionality.

T-Mobile is broadly aligned with above, but excludes Library Functions, Classes, SDK, Operating Systems, Networking Hardware type of entities from scope of APIs.

**T-Mobile's definition of an API:** A system access point that has a well-defined syntax and is accessible from application programs to provide well-defined functionality.

Creating an API
---------------

 _Why APIs?_

By exposing a software component functionality via a computing interface access point (API), opportunity arises for creating other applications and experiences, that are composed of networked interactions between multiple software applications or mixed hardware-software intermediaries. Through information hiding & abstraction, APIs enable modular programming, allowing users to use the interface independently of the implementation. 

### _Thinking of API as a Product_

While API encompass all sorts of access points, for example, internal components communication (e.g. micro service to micro service) of a domain product (applications), backend data sources communication (micro service to DB); there are significant benefits in elevating atleast a subset of those APIs into “**Productized APIs**”. When API’s intended consumer are external to the domain product (i.e. cross domain product), treating the APIs as a product is advised. Thinking of API as a Product requires us to achieve:

1.  **API as a Contract** between Provider and a Consumer
2.  Ensuring **Autonomy** of Consumption
3.  **Design Consistency** and predictable behavior
4.  Lifecycle Management

### _Describing an API_

API Specification document explains how the API behaves and what to expect from the API. Typically, it captures the kinds of calls or requests that can be made, how to make them, the data formats that should be used, the conventions to follow, syntax to be used, functionality provided by the API, etc. 

### _API Implementation Examples_

While an API can be entirely custom or specific to a component, effort should be made to design it based on an industry-standard to ensure interoperability.

Few widely used Industry Standards defining architecture style and implementation patterns:

REST  
SOAP  
REST-RPC  
SOAP-RPC  
GraphQL  
gRPC  
DB & data source interactions  
Message bus interactions  
Pub Sub: MQTT, AMQP, JMS  
Websockets

Web APIs

...

Few others...

COM  
dbus  
dcom  
ddl  
corba  
gsm command protocols  
tuxedo:   
bapi/idocs: SAP

...

### _API Description Examples_

API Specification document acts as a contract between an API provider and consumer, to enable decoupled evolution of provider and consumer implementation. It usually is an input for automated tools like automatic API documentation, SDK, client & server scaffold codes, Test Suite generators etc.

Few widely used Industry Standards for defining API Specification:

Open API Specification: in JSON/YAML notation, used with REST, REST-RPC.  
WSDL: Web Service Definition Language using XSD, used with SOAP, SOAP-RPC.  
GraphQL Schema, used with GraphQL.  
Protobufs: with gRPC

...

Others...

tuxedo header  
bapi/idocs descriptor files

...

Purpose
-------

The purpose of the API Design Standard is to

promote consistency across T-Mobile APIs

ease adoption for internal and external API consumers

enable the use of generic tests across REST resources

General Guidelines
------------------

This API standard adheres to these guidelines:

Treat APIs as a product with a dedicated owner rather than a mere integration layer

Build APIs following the [REST architectural style](https://www.ics.uci.edu/~fielding/pubs/dissertation/rest_arch_style.htm)

Create secure APIs following the [T-Mobile REST API Security Reference Architecture](../../sites/linked/referencearchitecture/referencearchitectureintro)

Comply with W3C and IETF standards for [URI](https://tools.ietf.org/html/rfc3986) and [HTTP](https://tools.ietf.org/html/rfc7231) as they are the core of REST

Support [JSON](http://www.json.org/) representation for all resources

Do not create a custom solution where an industry standard solution already exists

Do not force conformance to obsolete design patterns

Do what is best long-term for T-Mobile

Roles Impacted
--------------

The following roles are impacted by this standard:

App Ops and IT Ops

Collaboration Architects

Product Owners

Developers

Configuration Management

Domain Architects

Testers